#! /usr/bin/python3

# Document.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" Implement the base class for manipulating texts to be processed

    Each text must have a title and body
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
import hashlib

from typing import (
    Iterable,
    Dict,
    Union
)

# Additional packages
from oslash import (
    Maybe,
    Just,
    Nothing
)

# Project related packages
from cookbook.core.exceptions import PrivateAttributError

#############################################################################
#                                  Classes                                  #
#############################################################################

MightbeIterableString = Union[Iterable[str], str]


class Document():
    """ Implement the base class for manipulating texts to be processed.
        The class acts as a single entry point for the library

        Implementation details
        ======================
        * Implements a Private Data Class
    """

    __slots__ = ('_title', '_body', '_flags', '_uid')

    def __init__(self, title: MightbeIterableString, body: MightbeIterableString, flags=(), uid=None):
        """Default constructor.
        Arguments:
            title {MightbeIterableString} -- The title of the text, Nullable.
            body {MightbeIterableString} -- The body of the text. Not nullable.
        """

        if not body:
            raise TypeError('A valid body must be provided for a Document object to be created')

        self._title = self._text_factory(title)
        self._body = self._text_factory(body)
        self._flags = flags
        self._uid = uid

    # ---------------------------------------------- # Private

    def _text_factory(self, string: str) -> Maybe:
        """
        Default Monoid factory. Convert the raw string into a Monoid.
        The method can be Monkey Patched to dynamically add way to preprocess the text.
        I use Monkey Patching instead of inheritance to provide a light mechanism for extensibility.
        I'm still not sure if i'am going to hate me later for doing such a careless move !

        Arguments:
            string {str} -- The string to transform into a monood

        Returns:
            Maybe -- The resulting monid
        """

        if not string:
            out = Nothing()
        elif isinstance(string, Maybe):
            out = string
        else:
            try:
                out = Just(str(string))
            except TypeError:
                out = Nothing()

        return out

    # ---------------------------------------------- # Public

    def to_native(self) -> 'Document':
        """
        Return a Document object with title and body replaced as native python strucure, not monoides.
        """

        body = self.body.explode()
        title = self.title.explode()
        self._body = body
        self._title = title

        return self

    # ---------------------------------------------- # Protocol : String

    def __str__(self):
        """ String protocol
        TODO: Add support for iterable through singledispatch
        """

        return f""" 
        A document Object \n
        =================
        Title : {self._title} \n\n
        Body: {self._body}
        """

    # ---------------------------------------------- # Getters and setters

    @property
    def title(self):
        """
        Document's title getter
        """
        return self._title

    @title.setter
    def title(self, value):
        """
        Document's title setter. Raise an error, since the class is immutable
        """
        raise PrivateAttributError('`title` is an immutable attribute')

    # ---------------------------------------------- #

    @property
    def body(self):
        """
        Document's body getter
        """
        return self._body

    @body.setter
    def body(self, value):
        """
        Document's body setter. Raise an error, since the class is immutable
        """
        raise PrivateAttributError('`body` is an immutable attribute')

    # ---------------------------------------------- #

    @property
    def flags(self):
        """
        Document's flags getter
        """
        return self._flags

    @flags.setter
    def flags(self, value):
        """
        Document's flags setter. Raise an error, since the class is immutable
        """

        raise PrivateAttributError('`flags` is an immutable attribute')

    # ---------------------------------------------- #

    @property
    def uid(self):
        """
        Document's uid getters par setters
        If provided at object creation, the UID is returned. Otherwise, the
        UID is computed as hash of the text body
        """

        if self._uid is not None:
            uid = self._uid
        else:
            try:
                uid = self._body.bind(lambda x: hashlib.md5(x.encode('utf-8')).hexdigest())
            except AttributeError:
                uid = hashlib.md5(self._body.encode('utf-8')).hexdigest()
        return uid

    @uid.setter
    def uid(self, value):
        """
        Document's uid setter. Raise an error, since the class is immutable
        """

        raise PrivateAttributError('`uid` is an immutable attribute')

    # ---------------------------------------------- #  Statics

    @staticmethod
    def from_iterable(items: Iterable[Dict[str, str]]):
        """ Alternate constructor.
            Build a list of Documents objects from an iterator over a dictionnary.

        Arguments:
            items {Iterable[Dict[str, str]]} -- The iterator from which the Documents object will be build.
            Each of the dictionnary must have two keys : a title one, and a body one.

        Returns:
            Tuple[Document] -- A tuple of documents objects.
        """

        documents_list = []
        for item in items:
            try:
                documents_list.append(Document(item.title, item.body))
            except KeyError as error:
                logging.error('Item dictionnary must be like {title: <text\'s title>, body: <body\'s titile>}')
                raise error
            except OSError as error:
                raise error

        return tuple(documents_list)


if __name__ == "__main__":
    sys.exit()
