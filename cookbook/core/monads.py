#! /usr/bin/python3

# monads.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Monkey patch (temporary and ugly hack) the oslash module to attach unesting capabilities to the oslash monades
# TODO : Implements a proper inheritance for Just, List, Nothing and Mayby that matches the document's interface
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import Iterator

# Additional packages
from oslash import (
    Nothing,
    List,
    Just
)

#############################################################################
#                                  Classes                                  #
#############################################################################


# Explode

def _explode_nothing(self):
    return None


def explode_just(self):
    try:
        return self.value.explode()
    except AttributeError:  # Python primitive
        return self.value


def explode_list(self):
    """
    Recursively replace items of a monadic list with their values.

    Returns:
        list -- The replaced list
    """
    out = list()
    for item in self:
        try:
            out.append(item.explode())
        except AttributeError:
            out.append(item)  # Python primitive

    return [item for item in out if any(item)]


    # Register patches
Nothing.explode = _explode_nothing
Just.explode = explode_just
List.explode = explode_list


# Monkey patch the Iteration protocol to get ride of the Deprecation Warning
def list_iter(self) -> Iterator:
    """Return iterator for List."""

    xs = self  # Don't think we can avoid this mutable local
    while True:
        if xs.null():
            return  # to avoid deprecation warning raise StopIteration

        yield xs.head()
        xs = xs.tail()


List.__iter__ = list_iter


if __name__ == "__main__":
    sys.exit()
