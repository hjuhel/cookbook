#! /usr/bin/python3

# flags.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" Implements several flags to be retrieve, follow, and deal with interoperations
    dependencies
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys

# Additional packages
from flags import Flags

# Project related packages

#############################################################################
#                                  Classes                                  #
#############################################################################


# Steps Flags

class CookbookFlags(Flags):
    """
    Project wide flags 
    """
    pass


class PreprocessingFlags(CookbookFlags):
    """
    Sets when a preprocessing step is added
    """

    STEP_TOKENIZE = ()
    STEP_LEMMATIZE = ()
    STEP_NGRAMS = ()
    STEP_CHARNGRAMS = ()
    STEP_STOPWORDS = ()
    STEP_SENTENCES = ()


class HashFLags(CookbookFlags):
    """
    Sets when a hashing step is used
    """

    STEP_MINHASH = ()
    STEP_BANDINGBUCKETING = ()


class InformationExtractionFlags(CookbookFlags):
    """
    Sets when some information is extracted from a document
    """

    STEP_EMBED = ()

# Focus Flags


class ScopeFlags(CookbookFlags):
    """
    Use to set the focus of each step
    """

    TITLE = ()
    BODY = ()

# Utils Flags


class UtilsFlags(CookbookFlags):
    """
    Use to set utils flags
    """

    STEP_EXPAND = ()
    STEP_LAMBDA = ()


if __name__ == "__main__":
    sys.exit()
