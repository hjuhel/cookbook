#! /usr/bin/python3

# backend.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Implements a mixin to override the embedder of a backend with a Bert-as-service embedder
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from abc import (
    ABC,
    abstractmethod
)

# Additional packages

# Project related packages
from cookbook.core import Document
from .mixin_backend import MixinBackend


#############################################################################
#                                  Classes                                  #
#############################################################################


class BERTserviceMixin(MixinBackend, ABC):
    """
    Mixi. Override the embedder of a backend with a Bert-as-service embedder.

    Implementation details:
    ======================
    * Must be call before the backend class in the inheritancy list
    * The object thats inherit fromn the Mixin must call a super().__init__() after attribute assignement
    """

    @abstractmethod
    def __init__(self, service_ip: str, service_port: str):

        super().__init__()

    # ---------------------------------------------- #

    @abstractmethod
    def embed(self, document: Document) -> Document:
        """
        Transform a document to a list of ndimentional array.
        One array per token,
        One list of array per sentence

        Arguments:
            document {Document} -- The Document object to embed

        Returns:
            Document -- A document object in which both title and body are replaced
            with a nested structure of ndarrays
        """

        raise NotImplementedError('BERT service embedder is not implemented yet')


if __name__ == "__main__":
    sys.exit()
