#! /usr/bin/python3

# mixin_backend.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implement a interface to define customs mixins to extends the functionnality of the backends
Mixins are preferred over composition, because create a custom backend is supposed to be a rare event
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from abc import (
    ABC,
    abstractmethod
)

# Additional packages

# Project related packages
from cookbook.core import Document

#############################################################################
#                                  Classes                                  #
#############################################################################


class MixinBackend(ABC):
    """
    Interface. Define customs mixins to extends the functionnality of the backends.
    Mixins are preferred over composition, because create a custom backend is supposed to be a rare event. 
    """

    @abstractmethod
    def __init__(self):
        """
        The mixin instanciator.

        Implementations details
        =======================
        * The custom backend must always inherit first from the mixin, before inheriting of the base backend
        * Always call a super().__init__() at the end of the mixin init call to ensure that attribute will be override

        """

        raise NotImplementedError('Must be implemented in the concrete mixins')

    # ---------------------------------------------- #


if __name__ == "__main__":
    sys.exit()
