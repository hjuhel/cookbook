#! /usr/bin/python3

# recipe.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements a way to register steps and to process a datasets based on these steps
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import (
    Union,
    Tuple
)

from functools import (
    partial
)

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.backend import TextBackend
from cookbook.core.exceptions import UnsettableAttributError, NoStepsRegisteredError
from cookbook.steps import Step
from cookbook.utils import NotifyList
from cookbook.core.monads import (
    Nothing,
    Just
)
from cookbook.core.flags import ScopeFlags


#############################################################################
#                                  Classes                                  #
#############################################################################


class Recipe():
    """
    Implements a way to register steps and to process a list of documents based on these steps
    """

    __slots__ = ('_backend', '_steps', '_flags', '_scope', '_remove_monoid')

    def __init__(self, backend: TextBackend, scope=ScopeFlags.BODY | ScopeFlags.TITLE, remove_monoid=True):
        """
        Instanciate a new recipe object on which the steps will be registered.
        Once all the steps has been registered, the recipe can be used on a list
        of Document object, to `bake` the datas.

        Arguments:
            bakend {textBackend} -- The NLP bakend to use to process the text
            focus -- Flags used to control which parts of the document must be processed
            remove_monoid {bool} -- Schould the ouput of the recipe be expressed as monoid

        Implementation details
        ======================
        * The Recipe object uses an Observer to dynamically bind the steps
        * The Recipe object is used a a Mediator object for the steps and the engine
        * The Recipe object uses a Memento to keep a trace of the successive steps from the baking methods
        * Flags are used to combine steps, check for steps compatibility and dynamic dispatch

        # TODO: To avoid reimplementing all the iteration / slicing / add and sub protocols, Try with Recipe inherinting from Notify list instead of using it an attribute
        # TODO: Implement the step removal both from the ID and the step object
        # TODO: Check and ensure insertion/removal order of the steps and their corresponding flags (using the idx for the flag)
        # TODO: Rework callback to implements the Dependency flags's check
        """

        # Attributes check:
        if not isinstance(backend, TextBackend):
            raise TypeError(f'Recipe\'s backend must be of type TextBackend. The provided one if of class {type(backend).__name__} ')

        # Create callbacks to keep the states consistent between the steps and the recipe
        # Functions are partialed to allow the callback to keep an encapsulated reference of the Recipe instance it's belongs to.
        # If not partialed, the functions are always going to be remapped to the new recipe, every single time the object is recreated
        # Two altertnatives are : higher scope level definitions (cons : name pollution), or Borg pattern (cons: only singleton)
        def callback_bind_recipe_to_step(step, recipe):  # Bind the recipe to the step
            step._recipe = recipe

        def callback_unbind_recipe_from_step(step):  # Unbind the recipe from the engine
            step._recipe = None

        def callback_add_flag_from_step(step, recipe):  # Add flag to recipe
            recipe._flags = recipe._flags + (step.FLAG,)

        def callback_remove_flag_from_step(step, recipe):  # Remove flag from the recipe
            recipe._flags.remove(step.flag)

        # Attributes setting
        self._backend: TextBackend = backend
        self._steps: NotifyList[Step] = NotifyList()
        self._flags: Tuple = ()
        self._scope = scope
        self._remove_monoid = remove_monoid

        # Registering callback on the Observer
        self._steps.register_callback_on_addition(partial(callback_bind_recipe_to_step, recipe=self))
        self._steps.register_callback_on_deletion(partial(callback_unbind_recipe_from_step, recipe=self))
        self._steps.register_callback_on_addition(partial(callback_add_flag_from_step, recipe=self))
        self._steps.register_callback_on_deletion(partial(callback_remove_flag_from_step, recipe=self))

    # ---------------------------------------------- #  Getter / Setter

    @property
    def steps(self):
        """
        Steps's getter.
        """

        return self._steps

    @steps.setter
    def steps(self, value):
        """
        Steps's setter
        """
        raise UnsettableAttributError('You cannot directly modify the steps. Please use `<recipe object> + | - <step>` to modify the recipe')

    # ---------------------------------------------- #

    @property
    def scope(self):
        """
        Scope's attribute getter
        """

        return self._scope

    @scope.setter
    def scope(self, value):
        """
        Scope's setter

        Arguments:
            value {ScopeFlags} -- The new flags combination
        """

        self._scope = value
        return None

    # ---------------------------------------------- #

    @property
    def flags(self):
        """
        Flags's getter.
        """

        return self._flags

    @steps.setter
    def steps(self, value):
        """
        Flags's setter
        """
        raise UnsettableAttributError('You cannot directly modify the flags. Please use `<recipe object> + | - <step>` to modify the recipe')

    # ---------------------------------------------- #

    @property
    def backend(self):
        """
        Backend's getter.
        """

        return self._backend

    @steps.setter
    def steps(self, value):
        """
        Backend's setter
        """
        raise UnsettableAttributError('You cannot directly modify the backend. Please recreate a new Recipe')

    # ---------------------------------------------- #  Public

    def prepare(self, document):
        """
        Prepare the recipe by `training` it on a dataframe.
        Data resulting from the prepare will later be used as references objects.

        Arguments:
            document {Union[List[Document], Document]} -- The data on which the object must be `train`

        Implementation details
        ======================
        * The outers looper is performed on the steps rather than the datas to allow for trainnable steps
        to perform the computation
        """

        output = document
        # Document monadification:

        if not self.steps:
            raise NoStepsRegisteredError()

        for operation in self.steps:
            output = operation(output)

        if self._remove_monoid:
            output = tuple(item.to_native() for item in output)

        return output

    # ---------------------------------------------- #

    def bake(self, document):
        """
        Apply a new dataset on a `prepared` recipe

        Arguments:
            document {Union[List[Document], Document]} -- The data on which the recipe must be applyied

        Returns:
            Union[List[Document], Document] -- The data resulting from the recipe application
        """

        raise NotImplementedError('Moups')

    # ---------------------------------------------- #  Protocols

    def __str__(self):

        raise NotImplementedError('Will be implmenteted with the list of steps')

    # ---------------------------------------------- #

    def __add__(self, step_to_add: Step):
        """
        Register a new step in the current recipe

        Arguments:
            step {Step} -- The step to be added in the recipe. Steps will be executed the same way they are registered.

        Returns:
            Recipe -- The recipe with the newly registered steps
        """

        # Ensure that the step_to_add is of a valid type
        if not isinstance(step_to_add, Step):
            raise TypeError(f'Unsupported operand type : `{type(step_to_add)}` for +: `Recipe`')

        self._steps.append(step_to_add)

        return self

    # ---------------------------------------------- #

    def __sub__(self, step_to_remove: Step):
        """
        Unregister a step from the current Recipe

        Arguments:
            step_to_remove {step} -- The step object to remove
        """

        # Ensure that the step_to_add is of a valid type
        if not isinstance(step_to_remove, Step):
            raise TypeError(f'Unsupported operand type : `{type(step_to_remove)}` for +: `Recipe`')

        self._steps.remove(step_to_remove)

        return self

    # ---------------------------------------------- #  Private


if __name__ == "__main__":
    sys.exit()
