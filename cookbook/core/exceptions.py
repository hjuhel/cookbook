#! /usr/bin/python3

# privateData.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
    Implements exceptions raised by the library
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging

# Additional packages

# Project related packages

#############################################################################
#                                  Classes                                  #
#############################################################################


class PrivateAttributError(Exception):
    """
    Raised when using a setter on an attribute flagged as private
    """

    def __init__(self, message=None, errors=None, log=True):

        super().__init__(message, errors)
        self.message = message
        self.errors = errors
        if log:
            logging.error(self)

    def __str__(self):
        if self.message:
            info = f's{self.message}.'
        else:
            info = f'Cannot access to a private attribute.'

        return info

    # ---------------------------------------------- #


class UnsettableAttributError(Exception):
    """
    Raised when a setter is used on an attribute that is mutable but not directly settable.
    If raised, tje user schould update the attribute using the methods of the underlaying object itself.
    """

    def __init__(self, message=None, errors=None, log=True):

        super().__init__(message, errors)
        self.message = message
        self.errors = errors
        if log:
            logging.error(self)

    def __str__(self):
        if self.message:
            info = f's{self.message}.'
        else:
            info = f'Non-directly alterable attribute.'

        return info

    # ---------------------------------------------- #


class IncompatibleStepsChainingError(Exception):
    """
    Raised when two consecutives steps can't be chainned together
    """

    def __init__(self, message=None, errors=None, log=True):

        super().__init__(message, errors)
        self.message = message
        self.errors = errors
        if log:
            logging.error(self)

    def __str__(self):
        if self.message:
            info = f's{self.message}.'
        else:
            info = f'Non-chainable steps.'

        return info


class NotTrainableError(Exception):
    """
    Raised when the is_trained attribute of a step flagged as non trainable is switch
    """

    def __init__(self, message=None, errors=None, log=True):

        super().__init__(message, errors)
        self.message = message
        self.errors = errors
        if log:
            logging.error(self)

    def __str__(self):
        if self.message:
            info = f's{self.message}.'
        else:
            info = f'Non-trainable step.'

        return info


class NoStepsRegisteredError(Exception):
    """
    Raised when a recipe object is called without any registered steps
    """

    def __init__(self, message=None, errors=None, log=True):

        super().__init__(message, errors)
        self.message = f'Can\'t prepare or bake a recipe without any registered steps.'
        self.errors = errors

    def __str__(self):
        return self.message


class EmptyStopWordsListError(Exception):
    """
    Raised when the remove stop words methods is used on a empty stop words list
    """

    def __init__(self, errors=None, log=True):

        message = 'Can\'t perform stop words removal withe an empty list of tokens'
        super().__init__(message, errors)
        self.message = message
        self.errors = errors
        if log:
            logging.error(self)

    # ---------------------------------------------- #


class NotAMonadicObjectError(Exception):
    """
    Raised when an object that schould be a monad is NOT a monade
    """

    def __init__(self, message, errors=None, log=True):

        super().__init__(message, errors)
        self.message = message
        self.errors = errors
        if log:
            logging.error(self)

    # ---------------------------------------------- #


if __name__ == "__main__":
    sys.exit()
