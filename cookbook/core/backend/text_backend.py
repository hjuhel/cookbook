#! /usr/bin/python3

# backend.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" Implement a interface for the backends
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from abc import (
    ABC,
    abstractmethod
)

# Additional packages

# Project related packages
from cookbook.core import Document

#############################################################################
#                                  Classes                                  #
#############################################################################


class TextBackend(ABC):
    """
        Interface. Define a unique interface to perfome a sets of operations
        against a text object
    """

    __slots__ = ('_stopwrods',)
    __shared_state = {}

    def __init__(self):
        """
        Arguments:
            stopwords {List[str]} -- The list of words to ignore
        """

        self.__dict__ = self.__shared_state

    # ---------------------------------------------- #

    @abstractmethod
    def tokenize(self, document: Document, **kwargs) -> Document:
        """
        Return a new document object with the a tokenized version of
        the body and the title.
        The returned body and title are a tuple of strings

        Arguments:
            document {Document} -- The document object to process
        """

        raise NotImplementedError('Must be implemented in the concrete class')

    # ---------------------------------------------- #

    @abstractmethod
    def sentences(self, document: Document, **kwargs) -> Document:
        """
        Return a new document object with the body and title expressed as a tuple of sentences.

        Arguments:
            document {Document} -- The document object to process
        """

        raise NotImplementedError('Must be implemented in the concrete class')

    # ---------------------------------------------- #

    @abstractmethod
    def ngrams(self, document: Document, n=3, **kwargs) -> Document:
        """
        Return a Document object with set of ngrams for Title and Body.

        Arguments:
            document {Document} -- The Document object to process

        Keyword Arguments:
            n {int} -- The size of the ngrams (default: {3})

        Returns:
            Document -- The newly created Document
        """

        raise NotImplementedError('Must be implemented in the concrete class')

    # ---------------------------------------------- #

    @abstractmethod
    def charngrams(self, document: Document, n=3, **kwargs) -> Document:
        """
        Return a Document object with set of chararcter based ngrams for Title and Body.

        Arguments:
            document {Document} -- The Document object to process

        Keyword Arguments:
            n {int} -- The size of the charngrams (default: {3})

        Returns:
            Document -- The newly created Document
        """

        raise NotImplementedError('Must be implemented in the concrete class')

    # ---------------------------------------------- #

    @abstractmethod
    def embed(self, document: Document, **kwargs) -> Document:
        """
        Transform a document to a list of ndimentional array.
        One array per token,
        One list of array per sentence

        Arguments:
            document {Document} -- The Document object to embed

        Returns:
            Document -- A document object in which both title and body are replaced
            with a nested structure of ndarrays
        """

        raise NotImplementedError('Must be implemented in the concrete class')

    # ---------------------------------------------- #

    @abstractmethod
    def lemmatize(self, document: Document, **kwargs) -> Document:
        """Transform a document's body and title by replacing the word with their corresponding lemms

        Arguments:
            document {Document} -- The document object to lemmatize

        Returns:
            Document -- A documentin which both title and body has been lemmatized
        """

        raise NotImplementedError('Must be implemented in the concrete class')

    # ---------------------------------------------- #

    @abstractmethod
    def remove_stopWords(self, document: Document, **kwargs) -> Document:
        """Transform a document's object by removing the stop words from the title and the body

        Arguments:
            document {Document} -- The document object from which the stop words must be removed

        Returns:
            Document -- A document object with removed stop words
        """

        raise NotImplementedError('Must be implemented in the concrete class')


if __name__ == "__main__":
    sys.exit()
