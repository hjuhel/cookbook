#! /usr/bin/python3

# python_backend.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" Implement a Python Only Backend. Some functionnalities, like embedding might not be available
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
import re
from itertools import zip_longest, filterfalse
import typing as tp

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import PreprocessingFlags, ScopeFlags
from cookbook.core.exceptions import EmptyStopWordsListError
from cookbook.utils import preserve_atomic_dimentionality
from cookbook.core.monads import (
    Nothing,
    Just,
    List
)
from .text_backend import TextBackend

#############################################################################
#                                  Classes                                  #
#############################################################################


class PythonBackend(TextBackend):
    """
    Implements a text processor backend that only uses native python features
    """

    __slots__ = ('language', '_stop_words')
    REGEX_DICT = {
        'sentences': re.compile(r'([!.?]+)'),  # () is used to captures groups in the re.split function
        'capturing_tokens': re.compile(r'([!.?,;\',\"]+)|\s'),
        'non_capturing_tokens': re.compile(r'[!.?,;\',\"\s]+')
    }

    def __init__(self, stopwords: tp.List[str] = None):
        """Implement a Pyhton-only as a text processor.

        Keyword Arguments:
            language {str} -- The language model to use (default: {'en_core_web_sm'})

        Implementation details
        ======================
        * The class can easily be combined with add hoc extensions using a mixin based mechanism

        # TODO: Use a langage object to encapsulate the Stop Words and the regexs used to perform texts splits
        # TODO: Find a better way to deal with step dependencies than just checking for the flags
        """

        self._stop_words = stopwords if stopwords else []  # To avoid defaulting on a mutable collection
        super().__init__()

    # ------------------------------------------------------------ #

    def sentences(self, document: Document, scope) -> Document:
        """
        Return a Document object with title and body as a generator of sentences

        Arguments:
            document {Document} -- The document to process
            scope -- The operation scope

        Returns:
            Document -- A document object with title and body splitted by sentences
        """

        @preserve_atomic_dimentionality
        def sentences_(text: str):
            try:
                processed_text_splitted = (item for item in re.split(self.REGEX_DICT['sentences'], text) if item)
                processed_text = tuple("".join(item).strip() for item in zip_longest(*[processed_text_splitted, processed_text_splitted],
                                                                                     fillvalue=""))  # generator trick to bind together the sentente and the termination mark
                processed_text = tuple(item for item in processed_text if item)
                maybe_processed_text = List(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(sentences_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(sentences_) if ScopeFlags.BODY in scope else document.body

        flags = document.flags + (PreprocessingFlags.STEP_SENTENCES, )

        return Document(maybe_title, maybe_body, flags, document.uid)

    # ------------------------------------------------------------ #

    def tokenize(self, document: Document, scope) -> Document:
        """Return a Document object with title and and body tokenized

        Arguments:
            document {Document} -- The Document object to tokenized
            scope -- The operation scope

        Returns:
            Document -- A Document object with title and body as tuple of tokens
        """

        @preserve_atomic_dimentionality
        def tokenize_(text):
            try:
                processed_text = tuple(item for item in re.split(self.REGEX_DICT['capturing_tokens'], text) if item)
                processed_text = tuple(item for item in processed_text if item)
                maybe_processed_text = List(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(tokenize_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(tokenize_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_TOKENIZE,), document.uid)

    # ------------------------------------------------------------ #

    def ngrams(self, document: Document, scope, n=3) -> Document:
        """
        Return a Document object with a list of ngrams for Title and Body.

        Arguments:
            document {Document} -- The Document object to process
            scope -- The operation scope

        Keyword Arguments:
            n {int} -- The size of the ngrams (default: {3})

        Returns:
            Document -- The newly created Document with title and body as Tuple(str)
        """

        @preserve_atomic_dimentionality
        def ngrams_(text):
            try:
                processed_text = tuple(item for item in zip_longest(
                    *(tuple(item for item in re.split(self.REGEX_DICT['non_capturing_tokens'], text) if item != '')[i:] for i in range(n)), fillvalue=''))
                maybe_processed_text = Just(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(ngrams_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(ngrams_) if ScopeFlags.BODY in scope else document.body

        doc = Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_NGRAMS,), document.uid)

        return doc

    # ---------------------------------------------- #

    def charngrams(self, document: Document, scope, n=3) -> Document:
        """
        Return a Document object with a list of ngrams for Title and Body.

        Arguments:
            document {Document} -- The Document object to process
            scope -- The operation scope

        Keyword Arguments:
            n {int} -- The size of the ngrams (default: {3})

        Returns:
            Document -- The newly created Document
        """

        @preserve_atomic_dimentionality
        def charngrams_(text):
            try:
                processed_text = tuple(item for item in zip_longest(*(text[i:] for i in range(n)), fillvalue='') if item)
                maybe_processed_text = Just(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(charngrams_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(charngrams_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_CHARNGRAMS,), document.uid)

    # ---------------------------------------------- #

    def embed(self, document: Document, scope) -> Document:
        """
        Transform a document'S body and title to a list of ndimentional array.

        Arguments:
            document {Document} -- The Document object to embed
            scope -- The operation scope

        Returns:
            Document -- A document object in which both title and body are replaced
            with a nested structure of ndarrays
        """

        raise NotImplementedError('The Python Backend does not implements an embedding method.')

    # ---------------------------------------------- #

    def lemmatize(self, document: Document, scope) -> Document:
        """Transform a document's body by replacing the word with their corresponding lemms

        Arguments:
            document {Document} -- The document object to lemmatize
            scope -- The operation scope

        Returns:
            Document -- A documentin which both title and body has been lemmatized
        """

        raise NotImplementedError('The Python Backend does not implements a lemmantize method.')

    # ---------------------------------------------- #

    def remove_stopWords(self, document: Document, scope) -> Document:
        """Transform a document's object by removing the stop words from the title and the body

        Arguments:
            document {Document} -- The document object from which the stop words must be removed
            scope -- The operation scope

        Returns:
            Document -- A document object with removed stop words
        """

        if not self._stop_words:
            raise EmptyStopWordsListError()

        @preserve_atomic_dimentionality
        def remove_stopWords_(text, doc_flags):
            try:
                if PreprocessingFlags.STEP_TOKENIZE in doc_flags:   # Check if the document has already been tokenized
                    maybe_processed_text = Just(text) if text not in self._stop_words else Nothing()
                else:
                    processed_text = " ".join((item for item in filterfalse(lambda x: x in self._stop_words, re.split(
                        self.REGEX_DICT['capturing_tokens'], text)) if item))  # First split tokens before checking for stop words
                    maybe_processed_text = Just(processed_text) if processed_text else Nothing()  # Monadification

            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(lambda x: remove_stopWords_(x, document.flags)) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(lambda x: remove_stopWords_(x, document.flags)) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_STOPWORDS,), document.uid)


if __name__ == "__main__":
    sys.exit()
