#! /usr/bin/python3

# spacy_backend.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" Implement a Spacy backend to be used as a text processor
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
from itertools import filterfalse, zip_longest

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import PreprocessingFlags, InformationExtractionFlags, ScopeFlags
from cookbook.core.monads import (
    Nothing,
    List,
    Just
)  # CAVEAT : Oslshah monkey patching

from cookbook.utils import preserve_atomic_dimentionality
from .text_backend import TextBackend

#############################################################################
#                                  Classes                                  #
#############################################################################


class SpacyBackend(TextBackend):
    """
    Implements a Spacy backend to be used as a text processor
    """

    __slots__ = ('language',)

    def __init__(self, language: str = 'en_core_web_sm'):
        """Implement a Spacy Object as a text backend

        Keyword Arguments:
            language {str} -- The language model to use (default: {'en_core_web_sm'})

        Implementation details
        ======================
        * The class can easily be combined with add hoc extensions using a mixin based mechanism

        # TODO: Adding a cache mechanism to avoid aving to reinstanciate the NLP operation on each methods call
        # TODO: Find an intelligent way to proapage the dependencies between the tasks
        """

        super().__init__()

        try:
            import spacy
            logging.info(f"Loading spacy model `{language}`")
            self._nlp = spacy.load(language)
        except ModuleNotFoundError as error:
            logging.critical('`Spacy` must be installed for the spacy backend to be used')
            raise error
        except OSError as error:
            logging.critical(f'Spacy can\'t load {language}')

    # ------------------------------------------------------------ #

    def sentences(self, document: Document, scope) -> Document:
        """
        Return a Document object with title and body as a generator of sentences

        Arguments:
            document {Document} -- The document to process

        Returns:
            Document -- [description]
        """

        @preserve_atomic_dimentionality
        def sentences_(text: str):
            try:
                processed_text = tuple(item.text for item in self._nlp(text).sents if item)
                processed_text = tuple(item for item in processed_text if item)
                maybe_processed_text = List(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(sentences_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(sentences_) if ScopeFlags.BODY in scope else document.body

        flags = document.flags + (PreprocessingFlags.STEP_SENTENCES, )

        return Document(maybe_title, maybe_body, flags, document.uid)

    # ------------------------------------------------------------ #

    def tokenize(self, document: Document, scope) -> Document:
        """Return a Document object with title and and body tokenized

        Arguments:
            document {Document} -- The Document object to tokenized

        Returns:
            Document -- A Document object with title and body as tuple of tokens
        """

        @preserve_atomic_dimentionality
        def tokenize_(text):
            try:
                processed_text = tuple(item.text for item in self._nlp(text) if item)
                processed_text = tuple(item for item in processed_text if item)
                maybe_processed_text = List(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(tokenize_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(tokenize_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_TOKENIZE,), document.uid)

    # ------------------------------------------------------------ #

    def ngrams(self, document: Document, scope, n=3) -> Document:
        """
        Return a Document object with a list of ngrams for Title and Body.

        Arguments:
            document {Document} -- The Document object to process

        Keyword Arguments:
            n {int} -- The size of the ngrams (default: {3})

        Returns:
            Document -- The newly created Document with title and body as Tuple(str)
        """

        @preserve_atomic_dimentionality
        def ngrams_(text):
            try:
                processed_text = tuple(item for item in zip_longest(*(tuple(item.text for item in self._nlp(text)
                                                                            [i:] if any([item.is_alpha, item.is_digit, item.is_currency])) for i in range(n)), fillvalue=''))
                maybe_processed_text = Just(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(ngrams_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(ngrams_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_NGRAMS,), document.uid)

    # ---------------------------------------------- #

    def charngrams(self, document: Document, scope, n=3) -> Document:
        """
        Return a Document object with a list of ngrams for Title and Body.

        Arguments:
            document {Document} -- The Document object to process

        Keyword Arguments:
            n {int} -- The size of the ngrams (default: {3})

        Returns:
            Document -- The newly created Document
        """

        @preserve_atomic_dimentionality
        def charngrams_(text):
            try:
                processed_text = tuple(item for item in zip_longest(*(text[i:] for i in range(n)), fillvalue='') if item)
                maybe_processed_text = Just(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(charngrams_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(charngrams_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_CHARNGRAMS,), document.uid)

    # ---------------------------------------------- #

    def embed(self, document: Document, scope) -> Document:
        """
        Transform a document'S body and title to a list of ndimentional array.

        Arguments:
            document {Document} -- The Document object to embed

        Returns:
            Document -- A document object in which both title and body are replaced
            with a nested structure of ndarrays
        """

        @preserve_atomic_dimentionality
        def embed_(text):
            try:
                processed_text = self._nlp(text).vector
                maybe_processed_text = Just(processed_text) if processed_text.size > 0 else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(embed_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(embed_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (InformationExtractionFlags.STEP_EMBED,), document.uid)

    # ---------------------------------------------- #

    def lemmatize(self, document: Document, scope) -> Document:
        """Transform a document's body by replacing the word with their corresponding lemms

        Arguments:
            document {Document} -- The document object to lemmatize

        Returns:
            Document -- A documentin which both title and body has been lemmatized
        """

        @preserve_atomic_dimentionality
        def lemmatize_(text):
            try:
                processed_text = " ".join((tok.lemma_ for tok in self._nlp(text)))
                maybe_processed_text = Just(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(lemmatize_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(lemmatize_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_LEMMATIZE,), document.uid)

    # ---------------------------------------------- #

    def remove_stopWords(self, document: Document, scope) -> Document:
        """Transform a document's object by removing the stop words from the title and the body

        Arguments:
            document {Document} -- The document object from which the stop words must be removed

        Returns:
            Document -- A document object with removed stop words
        """

        @preserve_atomic_dimentionality
        def remove_stopWords_(text):
            try:
                processed_text = " ".join(tok.text for tok in filterfalse(lambda x: x.is_stop, self._nlp(text)) if tok is not None and tok.text != '')
                maybe_processed_text = Just(processed_text) if processed_text else Nothing()
            except BaseException:
                maybe_processed_text = Nothing()

            return maybe_processed_text

        maybe_title = document.title.map(remove_stopWords_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(remove_stopWords_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (PreprocessingFlags.STEP_STOPWORDS,), document.uid)


if __name__ == "__main__":
    sys.exit()
