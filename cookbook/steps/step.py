#! /usr/bin/python3

# step.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Implement an interface for the steps
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
from abc import (
    ABC,
    abstractmethod
)

from typing import (
    Union,
    Iterable,
    Generator,
    Callable
)

from multiprocessing import cpu_count

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import ScopeFlags
from cookbook.utils import method_dispatch  # Because static can't be used with single dispatch
from cookbook.core.exceptions import (
    NotTrainableError,
    IncompatibleStepsChainingError
)

#############################################################################
#                                  Classes                                  #
#############################################################################


class Step(ABC):
    """
    Interface. Define a unique interface to perform a specific operation on a Document object
    against a text object

    Implementation details
    ======================
    * The step must be implemented as a functor
    * The step must be idempotent, or explicitely looks in the Document.flags attribute to ensure data consistency

    # TODO: Implements depends on flags mechanism in the recipe object throught callbalck
    """

    #    __slots__ = ('_recipe', 'TRAINABLETEST', 'FLAG', '_DEPENDS_ON_FLAGS')  # Can't having both instance variables with values and slots ;(

    FLAG = None  # The unique flags identifying the operation. Must be registered in core.flags
    TRAINABLE = False  # Flag the step as trainable. If the step is trainable, the recipe.prepare methods will store the data resulting from the step
    _DEPENDS_ON_FLAGS = ()  # A tuple of flags on with the step relay
    _CORE_NUMBER = cpu_count() - 2  # Used for multiprocessing steps

    # ---------------------------------------------- #

    @abstractmethod
    def __init__(self, multiprocessing: bool = False, is_trained: bool = False, scope=None, *args, **kwargs):
        """Instanciate and configure the step

        Keyword Arguments:
            multiprocessing {bool} - - Whether or not the step schould be multiprocessed. (default: {False})
            is_trained {bool} -- whether or not the step has been trained (default: {False})
        """

        super().__init__()

        self._recipe = None  # The recipe object will be set when registering the step
        self._multiprocessing = multiprocessing
        self._is_trained = is_trained
        self._scope = scope

    # ---------------------------------------------- # Getters

    @property
    def recipe(self):
        """
        Getter for the recipe object the step belongs to

        Returns:
            Recipe -- The object the step belongs to
        """
        return self._recipe

    # ---------------------------------------------- #

    @property
    def is_trained(self) -> bool:
        """
        Getter for trained status of the step

        Returns:
            Bool -- The training status of the step
        """
        return self._is_trained

    @is_trained.setter
    def is_trained(self, value: bool) -> None:

        if self.TRAINABLE:
            self._is_trained = value
        else:
            raise NotTrainableError(f'{type(self).__name__} is not trainable')

    # ---------------------------------------------- #

    @property
    def scope(self) -> ScopeFlags:
        """
        Get the step's scope. Return the Recipe's scope if the steps one is not set

        Returns:
            ScopeFlags -- The flag scope of the step
        """

        if self._scope is not None:
            out = self._scope
        else:
            out = self.recipe.scope

        return out

    @scope.setter
    def scope(self, value) -> None:
        """
        Step's scope setter

        Arguments:
            value {ScopeFocusFlag} -- Control the scope of the flag. Override the one provided in the recipe
        """

        self._scope = value

        return None

    # ---------------------------------------------- # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]], callable_: Callable, *args, **kwargs) -> Union[Document, Iterable[Document]]:
        """
        Implements the functor protocol.
        Implements the concrete operation performed on the Document object
        Fallback / naïve method to apply a callable on list of documents. Can be used by inherited steps, or completely rewrited

        Arguments:
            document {Document} -- The document to process
            callable_ {Callable} -- The callable to apply on each document
            args -- Optionnal positionnal arguments to be passed to the callable. The callable can also be partialed
            kwargs -- Optionnal named arguments to be passed to the callable. The callable can also be partialed

        Returns:
            Document -- Return a NEW processed document
        """

        out = []
        try:
            for doc in self.ensure_generator(document):
                out.append(callable_(doc, *args, **kwargs))
        except BaseException as error:
            # msg = f'{callable_.__name__} step failed with error message : {error}'
            #raise IncompatibleStepsChainingError(msg)
            logging.error(error)  # msg
            raise error

        return tuple(out)

    # ---------------------------------------------- # Statiques

    @method_dispatch
    def ensure_generator(self, items):
        """
        Transform items into a generator
        Can be used by non trainable steps that does not needed all the dataset to be processed at once

        # TODO: Maybe add a data type check triggered by a debug flag
        """
        raise TypeError(f'Objects used in a recipe objects must be Documents objects or Iterable of Documents, not {type(items).__name__}')

    @ensure_generator.register(Document)
    def _(self, item):
        yield item

    @ensure_generator.register(tuple)
    @ensure_generator.register(list)
    @ensure_generator.register(map)
    @ensure_generator.register(Generator)
    @ensure_generator.register(zip)
    def _(self, items):
        for item in items:
            yield item


if __name__ == "__main__":
    sys.exit()
