#! /usr/bin/python3

# step_stopWords.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements the stop words removing procerdure
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import (
    Union,
    Iterable
)
import logging
from functools import partial

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import PreprocessingFlags
from cookbook.steps import Step


#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepStopWords(Step):
    """
    Perform the stop worlds removal operaiton
    """

    FLAG = PreprocessingFlags.STEP_STOPWORDS
    TRAINABLE = False

    def __init__(self, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate a stop words removal operation

        Keyword Arguments:

        Returns:
            Document -- A document with document's body and title from which stop words has been removed

        Implementation details
        ======================
        * The algorithms actually directly relays on the backend stop words removal implementation
        """

        if multiprocessing:
            logging.warning('Multiprocessing is not available for step `StepStopWords`')

        super().__init__(False, scope=scope)

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body has been 
        replaced with title and body without stop words

        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with body and title from which stop words has been removed
        """

        stop_words_ = partial(self.recipe.backend.remove_stopWords, scope=self.scope)
        return super().__call__(document, stop_words_)

    # ------------------------------------------------------------ # Private


if __name__ == "__main__":
    sys.exit()
