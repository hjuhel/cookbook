#! /usr/bin/python3

# step_charngrams.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements the charngrams extraction as a step, but it's actually the duty of the backend to implement the charngrams process
The class is only available for consistency
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import (
    Union,
    Iterable
)
from functools import partial

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import PreprocessingFlags
from cookbook.steps import Step

#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepCharngrams(Step):
    """
    Perform ngrams extraction
    """

    FLAG = PreprocessingFlags.STEP_CHARNGRAMS
    TRAINABLE = False

    def __init__(self, size=3, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate a charngrams extraction to be applied on Document

        Keyword Arguments:

        Returns:
            Document -- A document with document and title expressed tuple of tokens

        Implementation details
        ======================
        * The algorithms actually directly relays on the backend charngrams extraction's implementation
        """

        super().__init__(multiprocessing, scope=scope)
        self._size = size

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been replaced with a tuple of ngrams


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with tuples of grams
        """

        ngrams = partial(self.recipe.backend.charngrams, n=self._size, scope=self.scope)
        return super().__call__(document, ngrams)

    # ------------------------------------------------------------ # Private


if __name__ == "__main__":
    sys.exit()
