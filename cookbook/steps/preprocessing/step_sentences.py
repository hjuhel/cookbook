#! /usr/bin/python3

# step_sentences.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements the sentences extraction as a step, but it's actually the duty of the backend to implement the steps splitting process
The class is only available for consistency
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
from typing import (
    Union,
    Iterable
)
from functools import partial

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import PreprocessingFlags
from cookbook.steps import Step


#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepSentences(Step):
    """
    Perform sentences extraction
    """

    FLAG = PreprocessingFlags.STEP_SENTENCES
    TRAINABLE = False

    def __init__(self, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate a sentences extraction to be applied on Document

        Keyword Arguments:

        Returns:
            Document -- A document with document and title expressed as a list of sentences

        Implementation details
        ======================
        * The algorithms actually directly relays on the backend sentences extraction's implementation
        """

        if multiprocessing:
            logging.warning('Multiprocessing is not available for step `StepSentences`')

        super().__init__(False, scope=scope)

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been replaced with a tuple of ngrams


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with tuples of grams
        """

        sentences = partial(self.recipe.backend.sentences, scope=self.scope)
        return super().__call__(document, sentences)

    # ------------------------------------------------------------ # Private


if __name__ == "__main__":
    sys.exit()
