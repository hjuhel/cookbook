#! /usr/bin/python3

# step_lemmatize.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements the lemmatization of the document as a step, but it's actually the duty of the backend to implement the lemmatization process
The class is only available for consistency
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import (
    Union,
    Iterable
)
from functools import partial

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import PreprocessingFlags
from cookbook.steps import Step


#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepLemmatize(Step):
    """
    Perform string lemmatization
    """

    FLAG = PreprocessingFlags.STEP_LEMMATIZE
    TRAINABLE = False

    def __init__(self, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate a Lemmatizer on a text

        Keyword Arguments:

        Returns:
            Document -- A document with lemmantized document's body and title

        Implementation details
        ======================
        * The algorithms actually directly relays on the backend lemmantizer's implementation
        """

        super().__init__(multiprocessing, scope=scope)

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been lemmantized

        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with lemmantized body and title
        """

        lemmatize = partial(self.recipe.backend.lemmatize, scope=self.scope)
        return super().__call__(document, lemmatize)

    # ------------------------------------------------------------ # Private


if __name__ == "__main__":
    sys.exit()
