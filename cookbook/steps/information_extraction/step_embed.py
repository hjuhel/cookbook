#! /usr/bin/python3

# step_embed.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements the embedding as a step, but it's actually the duty of the backend to implement the embedding process
The class is only available for consistency
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from itertools import islice
import logging

from typing import (
    Iterable,
    Tuple,
    Union
)

# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import InformationExtractionFlags
from cookbook.steps import Step

#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepEmbed(Step):
    """
    Implements the embedding as a step, but it's actually the duty of the backend to implement the embedding process
    The class is only available for consistency
    """

    FLAG = InformationExtractionFlags.STEP_EMBED
    TRAINABLE = False

    def __init__(self, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate an embedding step to extract meaningfull information from the text

        Returns:
            Document -- A document with document and title expressed as Vector of

        Implementation details
        ======================
        * The algorithm does not preserve the dimension accross multiple processed document
        """

        if multiprocessing:
            logging.warning('Multiprocessing is not implemented for `StepEmbedding`')

        super().__init__(multiprocessing, scope=scope)

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body has been replaced with
        their corresponding vectorial representation


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with title and body expressed as List of embedding vectors
        """

        return super().__call__(document, self.recipe.backend.embed, self.scope)


if __name__ == "__main__":
    sys.exit()
