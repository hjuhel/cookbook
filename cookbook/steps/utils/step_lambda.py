#! /usr/bin/python3

# step_lambda.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Map an arbitrary callable to the Doocument object, Can be used to wrapp any partialad function when instanciating a step is overkill
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
from typing import (
    Union,
    Iterable
)

from itertools import chain


# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import UtilsFlags, ScopeFlags
from cookbook.core.monads import Nothing, Just, List
from cookbook.core.exceptions import NotAMonadicObjectError
from cookbook.steps import Step
from cookbook.utils import preserve_atomic_dimentionality


#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepLambda(Step):
    """
    Apply an arbitrary callable on the title and body of each doc of the dataset
    """

    FLAG = UtilsFlags.STEP_LAMBDA
    TRAINABLE = False

    def __init__(self, callable_: callable, multiprocessing=False, scope=None, mode="append") -> Document:
        """
        Instanciate a Lambda step to apply an arbitrary callable to the tuples of documents

        Keyword Arguments:
            callable {callable} -- The callable to apply on the dataset. The output MUST be of a Monadic type as define in cookbook.core.monadic, otherwise, the chaining step will fails

        Returns:
            Document -- A document with document and title expressed as a an array


        # TODO: Add named lambda through ancillary datas in the callable
        """

        if multiprocessing:
            logging.warning('Multiprocessing is not available for step `StepLambda`')

        if mode not in ('extend', 'append'):
            raise ValueError("`mode` param must be one of ('extend', 'append')")

        super().__init__(False, scope=scope)
        self._mode = mode
        self._callable = callable_

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been replaced with a tuple of ngrams


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with tuples of grams
        """

        out = []
        mode = {'extend': out.extend,
                'append': out.append
                }

        try:
            for doc in self.ensure_generator(document):
                mode[self._mode](self._apply_callable(doc, self.scope))  # Extend / Append the list with the appropriate methode
        except TypeError as error:
            logging.error(error)  # msg
            raise TypeError('List expansion failed. Check if the object returned by the lambda is consistent with the `mode\'s param choosed.')
        except BaseException as error:
            logging.error(error)  # msg
            raise error

        return tuple(out)

    # ------------------------------------------------------------ # Private

    def _apply_callable(self, document: Document, scope: ScopeFlags) -> Iterable[Document]:
        """
        Apply the registered callable to the document dataset

        Arguments:
            document {Document} -- The document on which apply the callable
            scope {ScopeFlags} -- The Scope Controller

        Returns:
            Iterable[Document] -- The expanded iterable of Documents object
        """

        @preserve_atomic_dimentionality
        def _apply_callable_(data):
            try:
                maybe_processed = self._callable(data)
                if not isinstance(maybe_processed, (Nothing, Just, List)):
                    raise NotAMonadicObjectError('The output of a lambda applied throught a StepLambda must be one of thoose defines in cookbook.core.monad')
            except BaseException as error:
                logging.error(f'Lambda Step\'s application results in a failure with error : {error}')
                maybe_processed = Nothing()

            return maybe_processed

        maybe_title = document.title.map(_apply_callable_) if ScopeFlags.TITLE in scope else document.title
        maybe_body = document.body.map(_apply_callable_) if ScopeFlags.BODY in scope else document.body

        return Document(maybe_title, maybe_body, document.flags + (UtilsFlags.STEP_LAMBDA,), document.uid)


if __name__ == "__main__":
    sys.exit()
