#! /usr/bin/python3

# step_lambda.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Expand a list inside the title / the body of size n into a n-document objects, each with a title / body as Just
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import logging
from typing import (
    Union,
    Iterable
)

from functools import partial
from itertools import chain


# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import UtilsFlags, ScopeFlags
from cookbook.core.monads import Nothing
from cookbook.steps import Step
from cookbook.utils import preserve_atomic_dimentionality


#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepExpand(Step):
    """
    Perform sentences extraction
    """

    FLAG = UtilsFlags.STEP_EXPAND
    TRAINABLE = False

    def __init__(self, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate an array extending operation on the list of document

        Keyword Arguments:

        Returns:
            Document -- A document with document and title expressed as a an array
        """

        if multiprocessing:
            logging.warning('Multiprocessing is not available for step `StepExpand`')

        super().__init__(False, scope=scope)

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been replaced with a tuple of ngrams


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with tuples of grams
        """

        out = []
        try:
            for doc in self.ensure_generator(document):
                out.extend(self._expand_one_document(doc, self.scope))
        except BaseException as error:
            logging.error(error)  # msg
            raise error

        return tuple(out)

    # ------------------------------------------------------------ # Private

    def _expand_one_document(self, document: Document, scope: ScopeFlags) -> Iterable[Document]:
        """
        Expand one document into a list of n document based on the allarity of the title and the body

        Arguments:
            document {Document} -- The document to expand
            scope {ScopeFlags} -- The Scope Controller

        Returns:
            Iterable[Document] -- The expanded iterable of Documents object

        # TODO: Add a wayto handle re-uid strategy
        """

        out = []

        @preserve_atomic_dimentionality
        def expand_(data, target, flags):
            try:

                if target == ScopeFlags.TITLE:
                    maybe_processed = Document(title=data, body=Nothing(), flags=flags)
                elif target == ScopeFlags.BODY:
                    maybe_processed = Document(title=Nothing(), body=data, flags=flags)
                else:
                    maybe_processed = Nothing()

            except BaseException:
                maybe_processed = Nothing()

            return maybe_processed

        flags = document.flags + (UtilsFlags.STEP_EXPAND,)  # Extract the flags to be propagate to the newly created document
        maybe_title = document.title.map(lambda x: expand_(x, ScopeFlags.TITLE, flags)) if ScopeFlags.TITLE in scope else Nothing()  # doc->list[title] to list[doc->title_chunk]
        maybe_body = document.body.map(lambda x: expand_(x, ScopeFlags.BODY, flags)) if ScopeFlags.BODY in scope else Nothing()  # doc->list[body] to list[doc->body_chunk]

        exploded_body_and_titles = map(lambda x: x.explode() or [], (maybe_title, maybe_body))   # Remove the outermost monadic level
        out.extend(item for item in chain.from_iterable(exploded_body_and_titles) if item)

        return out


if __name__ == "__main__":
    sys.exit()
