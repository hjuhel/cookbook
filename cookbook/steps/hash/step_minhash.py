#! /usr/bin/python3

# step_minhash.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements a Min Hash algorithm
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from random import (
    seed,
    sample
)
import logging

from typing import (
    Tuple,
    Union,
    Iterable
)

from functools import singledispatch

# Additional packages
import numpy as np
import mmh3

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import HashFLags, ScopeFlags
from cookbook.core.exceptions import IncompatibleStepsChainingError
from cookbook.utils import preserve_atomic_dimentionality
from cookbook.steps import Step
from cookbook.core.monads import (
    Nothing,
    Just
)


#############################################################################
#                                Functions                                  #
#############################################################################


@singledispatch
def _ensure_iterable_of_string(item):
    """
    Transform the input into an iterable of string to ensure consistency
    accross steps

    Arguments:
        item {Any} -- The item to sanityze

    Raises:
        IncompatibleStepsChainingError: Raised if the output of the previous step
        cannot be transform into a valid input for the current step
    """
    raise IncompatibleStepsChainingError('Step preceding `MinHash` does not produce a MinHash compatible step')


@_ensure_iterable_of_string.register(str)
def _(item):
    yield item


@_ensure_iterable_of_string.register(tuple)
@_ensure_iterable_of_string.register(list)
def _(items):
    for item in items:
        yield str(item)


#############################################################################
#                                  Classes                                  #
#############################################################################


class StepMinHash(Step):
    """
    Implements the min hash algorithme
    """

    FLAG = HashFLags.STEP_MINHASH
    TRAINABLE = False

    def __init__(self, signature_size: int = 100, random_seed: int = 42, scope=None, XOR: bool = False) -> Document:
        """
        Instanciate a MinHash step. This step compute a MinHash signature for a Document.

        Keyword Arguments:
            random_seed {int} -- The master random seed to use. (default: {42})
            signature_size {int} -- The desired signature's size (default: {100})
            XOR {bool} -- Whether or not use the XOR mode. If False, multiple hash will be use, otherwise, the shingles will
            only be hashed once and then XORed with a random seed. The XOR process is about twice faster
            than the multiples hashes, but only get a distance of {0, 1} (default: {False})

        Returns:
            Document -- A document with document and title expressed as hash

        Implementation details
        ======================
        * The class implements the `K-Smallest-values` variation of the minhash algorithm
        """

        super().__init__(scope=scope)
        seed(random_seed)
        self._randoms_seeds = sample(range(1, int(signature_size)*int(signature_size) + 1), int(signature_size))
        self._XOR = XOR
        logging.info(f'Random_seed vector\'s sum is: {sum(self._randoms_seeds)}')

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been hashed using the min hash algorithms


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with two minhash signatures : one for the title and one for the body
        """

        @preserve_atomic_dimentionality
        def compute_min_hash_signature_(shingles_list):
            try:
                # Create on array of the text hashed
                shingles_to_array = np.array([mmh3.hash(shingle, self._randoms_seeds[0]) for shingle in _ensure_iterable_of_string(shingles_list)]).reshape(1, -1)

                # Create a signature-1 transformation of the hash, to approximate rehashing. The transformation are done using XOR, because accumulating hashes is slow.
                if self._XOR:
                    shingles_hashed = np.array([[shingle ^ seed for shingle in shingles_to_array] for seed in self._randoms_seeds[1:]]).reshape(len(self._randoms_seeds) - 1, -1)
                else:
                    shingles_hashed = np.array([[mmh3.hash(shingle, seed) for shingle in shingles_to_array[0]] for seed in self._randoms_seeds[1:]]).reshape(len(self._randoms_seeds) - 1, -1)

                # Compute the signature by computing the minimal hash/transformation for each transformations
                shingles_signature = np.min(np.concatenate((shingles_to_array, shingles_hashed), axis=0), axis=1)
                maybe_shingle_signature = Just(list(shingles_signature)) if shingles_signature.size > 0 else Nothing()

            except BaseException as error:
                maybe_shingle_signature = Nothing()

            return maybe_shingle_signature

        out = []
        for doc in document:
            maybe_title = doc.title.map(compute_min_hash_signature_) if ScopeFlags.TITLE in self.scope else doc.title
            maybe_body = doc.body.map(compute_min_hash_signature_) if ScopeFlags.BODY in self.scope else doc.body
            out.append(Document(maybe_title, maybe_body, doc.flags + (HashFLags.STEP_MINHASH,), doc.uid))

        return tuple(out)


if __name__ == "__main__":
    sys.exit()
