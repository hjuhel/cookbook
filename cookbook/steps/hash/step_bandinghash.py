#! /usr/bin/python3

# step_bandinghash.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Bands and buckecting of iterators
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from itertools import islice
import logging

from typing import (
    Iterable,
    Tuple,
    Union
)

# Additional packages
import mmh3

# Project related packages
from cookbook.core import Document
from cookbook.core.flags import HashFLags, ScopeFlags
from cookbook.core.exceptions import IncompatibleStepsChainingError
from cookbook.steps import Step

#############################################################################
#                                Functions                                  #
#############################################################################

#############################################################################
#                                  Classes                                  #
#############################################################################


class StepBandingHash(Step):
    """
    Perform dimension reduction of iterable by slicing and hashing an iterator
    """

    FLAG = HashFLags.STEP_BANDINGBUCKETING
    TRAINABLE = False

    def __init__(self, banding_size: int = 2, random_seed: int = 42, multiprocessing=False, scope=None) -> Document:
        """
        Instanciate a banding and bucketing algorithm to reduce the dimensionality
        of a vector

        Keyword Arguments:
            banding_size {int} -- The size of the band
            random_seed {int} -- The master random seed to use. (default: {42})

        Returns:
            Document -- A document with document and title expressed as banded hashs

        Implementation details
        ======================
        * The algorithm does not preserve the dimension accross multiple processed document
        """

        if multiprocessing:
            logging.warning('Multiprocessing is not implemented for `StepBandingHash`')

        super().__init__(multiprocessing, scope=scope)

        self._random_seed = random_seed
        self._banding_size = int(banding_size)

    # ------------------------------------------------------------ # Protocols

    def __call__(self, document: Union[Document, Iterable[Document]]) -> Union[Document, Iterable[Document]]:
        """
        Process a Document object and return a Document object for which title and body
        has been hashed using the banding hash algorithms


        Arguments:
            document {Document} -- The Document object to process

        Returns:
            Document -- A Document object with two hash : one for the title and one for the body
        """

        out = []

        for doc in self.ensure_generator(document):
            title = self._compute_hash(doc.title) if ScopeFlags.TITLE in self.scope else doc.title
            body = self._compute_hash(doc.body) if ScopeFlags.BODY in self.scope else doc.body
            flags = doc.flags + tuple(HashFLags.STEP_BANDINGBUCKETING,)  # Append the flags to the list of processed flags

            out.append(Document(title=title, body=body, flags=flags, uid=doc.uid))

        return tuple(out)

    # ------------------------------------------------------------ # Private

    def _compute_hash(self, item: Iterable) -> Tuple[int]:
        """
        Compute the signature for a given iterator

        Arguments:
            item {iterator} -- The iterator to process

        Returns:
            Tuple[int] -- The resulting hash
        """

        item_hash = []
        try:
            for band in islice(item, 0, None, self._banding_size):
                item_hash.append(mmh3.hash(str(band), seed=self._random_seed))
        except TypeError as error:
            IncompatibleStepsChainingError(f'The required input of the `BandingHash` is not compatible with the outputed Document object of the previous step.')
            raise error

        return tuple(item_hash)


if __name__ == "__main__":
    sys.exit()
