#! /usr/bin/python3

# notify_list.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements an Observer pattern on a list object
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import Callable
from collections import UserList
from functools import wraps

# Additional packages

# Project related packages

#############################################################################
#                                Functions                                  #
#############################################################################


def _callback_method_on_addition(func: Callable) -> Callable:
    """
    Transform a callable to a callback for addition operation
    Callbacks are executed AFTER the insertion

    Arguments:
        func {Callable} -- The callable to wrap as a callback

    Returns:
        Callable -- The wraped callable
    """

    @wraps(func)
    def notify(self, *args, **kwargs):
        out = func(self, *args, **kwargs)
        for _, callback in self._callbacks_on_add:
            callback(*args, **kwargs)
        return out
    return notify


def _callback_method_on_deletion(func: Callable) -> Callable:
    """
    Transform a callable to a callback for deletion operation
    Callbacks are executed after the deletion

    Arguments:
        func {Callable} -- The callable to wrap as a callback

    Returns:
        Callable -- The wraped callable
    """

    @wraps(func)
    def notify(self, *args, **kwargs):
        out = func(self, *args, **kwargs)
        for _, callback in self._callbacks_on_del:
            callback(*args, **kwargs)
        return out
    return notify

#############################################################################
#                                  Classes                                  #
#############################################################################


class NotifyList(UserList):
    """Extend the built-in list.
    * The NotifyList can register callbacks to be called for each addition / deletion
    """

    # Override/decorating UserList methods to trigger the callbacks
    extend = _callback_method_on_addition(UserList.extend)
    append = _callback_method_on_addition(UserList.append)
    remove = _callback_method_on_deletion(UserList.remove)
    pop = _callback_method_on_deletion(UserList.pop)
    __delitem__ = _callback_method_on_deletion(UserList.__delitem__)
    __setitem__ = _callback_method_on_addition(UserList.__setitem__)
    __iadd__ = _callback_method_on_addition(UserList.__iadd__)
    __imul__ = _callback_method_on_addition(UserList.__imul__)

    def __getitem__(self, item):
        if isinstance(item, slice):
            item = self.__class__(UserList.__getitem__(self, item))
        else:
            item = UserList.__getitem__(self, item)

        return item

    def __init__(self, *args):
        UserList.__init__(self, *args)
        self._callbacks_on_add = []
        self._callbacks_on_del = []
        self._callback_cntr = 0

    def register_callback_on_addition(self, callback):
        self._callbacks_on_add.append((self._callback_cntr, callback))
        self._callback_cntr += 1
        return self._callback_cntr - 1

    def register_callback_on_deletion(self, callback):
        self._callbacks_on_del.append((self._callback_cntr, callback))
        self._callback_cntr += 1
        return self._callback_cntr - 1

    def unregister_callback_on_addition(self, cbid):
        out = None
        for idx, (i, callback) in enumerate(self._callbacks_on_add):
            if i == cbid:
                self._callbacks_on_add.pop(idx)
                out = callback
        return out

    def unregister_callback_on_deletion(self, cbid):
        out = None
        for idx, (i, callback) in enumerate(self._callbacks_on_del):
            if i == cbid:
                self._callbacks_on_del.pop(idx)
                out = callback
        return out


if __name__ == "__main__":
    sys.exit()
