#! /usr/bin/python3

# preserve_dimensionality.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements a decorator in charge of decoupling a Callable application from the dimentionality of the input by recusively calling itself on the input
The decorator can be used to abstract and preserve the dimentionality of potentialy nested input
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from functools import wraps, singledispatch
from typing import Generator


# Additional packages

# Project related packages
from cookbook.core import Document
from cookbook.core.monads import (
    Nothing,
    Just,
    List
)

#############################################################################
#                                Functions                                  #
#############################################################################


@singledispatch
def _apply_on_inner(items, callable_, *args, **kwargs):
    """
    Apply a callable to the inner registred item of items

    Arguments:
        items -- A potentially nested structure of item on which apply item
        callable_ {Callable} -- The callable to apply on the item
    """

    raise NotImplementedError(f'Not implemented for input of type {type(items).__name__}')


@_apply_on_inner.register(list)
def _(items, callable_, *args, **kwargs):
    return [_apply_on_inner(item, callable_, *args, **kwargs) for item in items]


@_apply_on_inner.register(tuple)
def _(items, callable_, *args, **kwargs):
    return tuple(_apply_on_inner(item, callable_, *args, **kwargs) for item in items)


@_apply_on_inner.register(Generator)
def _(items, callable_, *args, **kwargs):
    return (_apply_on_inner(item, callable_, *args, **kwargs) for item in items)


@_apply_on_inner.register(List)
def _(items, callable_, *args, **kwargs):
    return items.map(lambda x: _apply_on_inner(x, callable_, *args, **kwargs))  # Dimentionnality will be increase for each list


@_apply_on_inner.register(Just)
def _(item, callable_, *args, **kwargs):
    return item.bind(lambda x: callable_(x, *args, **kwargs))  # The callable MUST return a Monade. The dimentionnality won't be increase, since Just is considered Atomic


@_apply_on_inner.register(str)
def _(items, callable_, *args, **kwargs):
    return callable_(items, *args, **kwargs)

    # ---------------------------------------------- #


def preserve_atomic_dimentionality(callable_):
    """
    Decorator. Used to abstract the function application from the
    dimentionnality of the items the function must be applied on.

    Exemples
    ========
    g(f, x, n) -> f(x, n)
    g(f, [x], n) -> [f(x, n)]
    g(f, [[x], [y]], n) -> [[f(x, n)], [f(y, n )]]

    where n is a parameter of the function f

    Arguments:
        callable_ {Callable} -- The callable to apply on the inner most registered item
    """

    @wraps(callable_)
    def wrapper(document, *args, **kwargs):
        return _apply_on_inner(document, callable_, *args, **kwargs)
    return wrapper


if __name__ == "__main__":
    sys.exit()
