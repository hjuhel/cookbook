#! /usr/bin/python3

# method_dispatch.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implements functools.singledispatch for methods
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from functools import singledispatch, update_wrapper
from typing import Callable

# Additional packages

# Project related packages

#############################################################################
#                                Functions                                  #
#############################################################################


def method_dispatch(func: Callable) -> Callable:
    """
    Transform a method into a generic

    Arguments:
        func {Callable} -- The Callable to convert

    Returns:
        Callable -- An wrapped method which Generic abilities
    """
    dispatcher = singledispatch(func)

    def wrapper(*args, **kw):
        return dispatcher.dispatch(args[1].__class__)(*args, **kw)
    wrapper.register = dispatcher.register
    update_wrapper(wrapper, func)
    return wrapper


if __name__ == "__main__":
    sys.exit()
