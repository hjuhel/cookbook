#! /usr/bin/python3

# demo.cookbook.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Demo of some cookbook functionnalities. Cookbook provide an abstraction over Spacy / NLTK / GENSIM to
performs transformation and preprocessings on texts.
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
import re
from typing import List
from collections import defaultdict, Counter
from itertools import chain

# Additional packages

# Project related packages
from cookbook.core import Document, Recipe
from cookbook.core.backend import PythonBackend, SpacyBackend
from cookbook.core.flags import PreprocessingFlags, ScopeFlags
from cookbook.core.monads import Just, Nothing
from cookbook.steps.preprocessing import StepCharngrams, StepNgrams, StepStopWords, StepStopWords, StepSentences
from cookbook.steps.information_extraction import StepEmbed
from cookbook.steps.hash import StepMinHash
from cookbook.steps.utils import StepLambda

#############################################################################
#                                  Scripts                                  #
#############################################################################

################
# Quick How To #
################

# Instanciate a brunch of document.
doc_1 = Document(body="Once upon a midnight dreary, while I pondered, weak and weary, Over many a quaint and curious volume of forgotten lore While I nodded, nearly napping, suddenly there came a tapping, As of some one gently rapping, rapping at my chamber door. Tis some visitor, I muttered, tapping at my chamber doorOnly this and nothing more.", title=None)
doc_2 = Document(body="Ah, distinctly I remember it was in the bleak December. And each separate dying ember wrought its ghost upon the floor. Eagerly I wished the morrow vainly I had sought to borrow From my books surcease of sorrow—sorrow for the lost Lenore For the rare and radiant maiden whom the angels name Lenore Nameless here for evermore.", title=None)
docs = [doc_1, doc_2]

# Instanciate a Recipe object:
# - The recipe objects holds both the engine to process the texts, and the steps to be applied on the documents
# - For now, only a Spacy and a full Python backend are availables. Both are extensible with mixins

# The backend is set to be a Spacy backend (using an english model, trained on a tiny wiki corpus). All steps will only be executed on the body attribute, thanks to the scope controler
recipe = Recipe(SpacyBackend('en_core_web_sm'), scope=ScopeFlags.BODY)

# Add a first step to the recipe

recipe + StepStopWords()  # Stop words will be removed from the document

processed_docs = recipe.prepare(docs)  # Fit the recipe to the documents. The allarity schould be preserved
print(processed_docs[0].body)  # Check that the stopwords has been removed

# Add an other step to build a complete pipeline.
recipe + StepSentences()
processed_docs = recipe.prepare(docs)

print(processed_docs[0].body)  # Check that now, the output is a list with one item per sentence. The allarity has been natively handled by the step sentence

# Use the step lambda to apply an arbitrary callable on the document, to remove the punctuation parks


def remove_punct(x):
    try:
        out = Just(re.sub(r',', '', x))  # The callable must always return a Monad
    except BaseException:
        out = Nothing()

    return out


recipe + StepLambda(remove_punct)
processed_docs = recipe.prepare(docs)

print(processed_docs[0].body)  # Check that is not punctuation mark anmore.


####################
# Complete recipes #
####################

# Sometime, we want to build a complete pipeline :

# Remove the stop words and get an embedding per sentences. Thanks to the step Sentences, the processing will results in one array with one item (an embedding array) per sentence
embedd_per_sents = Recipe(SpacyBackend('en_core_web_sm'), scope=ScopeFlags.BODY) + \
    StepStopWords() + \
    StepSentences() + \
    StepEmbed()

embedd_whole_text = Recipe(SpacyBackend('en_core_web_sm'), scope=ScopeFlags.BODY) + \
    StepStopWords() + \
    StepEmbed()

processed_docs_sentence = embedd_per_sents.prepare(docs)
print(len(processed_docs_sentence[0].body))  # Schould be 2. One per sentence.
print(len(processed_docs_sentence[0].body[0]))  # Schould be 384

processed_docs_text = embedd_whole_text.prepare(docs)
print(len(processed_docs_text[0].body))  # Schould be 384, which is the embedding size


#########################################################
# Use the Recipe and the MinHash steps to compare texts #
#########################################################
# Thanks to the MinHash, i can compare C(2 N) texts without having to perform n^2/2 comparison

# We want to find duplicates in text based on their similarity, without having to explicitely compare them. I use the following
# recipe to encode the texts, and the extrac_cluster methods to retrieve cluster. I also use the uid propeprty of the document object
# This attribute, if sets, is preserved accross the whole document
recipe = Recipe(PythonBackend(), scope=ScopeFlags.BODY) + \
    StepNgrams() + \
    StepMinHash(signature_size=100)


def extrac_cluster(items: List[Document], recipe: Recipe, duplication_threshold: int) -> List[List[str]]:
    """
    Clusterize items into groups based on the detected number of cluster

    Arguments:
        items {List[ItemDocument]} -- The list of documents to process
        signature_size {int} -- The signature to use for the encoding
        duplication_threshold {int} -- The threshold to use for the jacquard index to consider two documents as duplicates.

    Returns:
        List[List[str]] -- A list of cluster. Each cluster consists in a list of document's uid

    Implementaiton details
    ======================
    The method use an inverted index, based on the encoding of the docs
    """

    # Extract the hash from the texts
    signatures = recipe.prepare(items)
    signatures = tuple(item for item in signatures if item)  # Only keep the body: Nothing() from corpus

    # Deal with polar cases
    if not signatures:
        return []
    if len(signatures) == 1:
        return [signatures[0].uid]

    # Build a key 2 doc to avoid useless comparison : Only document sharing at one least one key will be compared together
    doc2key = defaultdict(set)
    for doc in signatures:
        for key in doc.body:
            doc2key[key].add(doc.uid)

    # Traverse all documents to retrieve potential duplicates for a specific document
    clusters = []  # A list of retrieved clusters
    for doc in signatures:
        # Check if the doc has already been added in a cluster, if so go the next document:
        if doc.uid in chain.from_iterable(clusters):
            continue

        counter = Counter(chain.from_iterable(doc2key[key] for key in doc.body))  # Build an occurencies counter of coocurences between the docs and all others docs
        cluster = {item[0] for item in counter.items() if item[1] >= duplication_threshold}  # Select the docs'ID for which the occurences is larger than the threshold
        cluster.add(doc.uid)  # Add the uid of document acting as a reference
        clusters.append(list(cluster))

        # Remove the already added document from the dict :
        # The left difference is the the key that might be shared by other documents than with the reference doc, so i can't just remove the dict's key
        # Remove the ref document
        for key in doc.body:
            doc2key[key] = doc2key[key] - cluster  # Even if the new set is empty, the key is preserved to avoid dealing with KeyError in the counter (it's faster)

    return clusters


# Exemples documents : the doc_2 doc, is alighty modified doc_1.
doc_1_cluster_a = Document(body="Once upon a midnight dreary, while I pondered, weak and weary, Over many a quaint and curious volume of forgotten lore While I nodded, nearly napping, suddenly there came a tapping, As of some one gently rapping, rapping at my chamber door. Tis some visitor, I muttered, tapping at my chamber doorOnly this and nothing more.",
                           title=None,
                           uid="doc_1_cluster_a"
                           )
doc_2_cluster_a = Document(body="perturbation On upon a midnight dreary,  I pondered, weak and weary, Over  a quaint and curious volume of  lore  I nodded, nearly napping, suddenly  came a tapping, As of some one gently rapping, rapping at my  door. Tis some visitor, I muttered, tapping  my chamber  this and nothing more.",
                           title=None,
                           uid="doc_2_cluster_a"
                           )

doc_1_cluster_b = Document(body="Ah, distinctly I remember it was in the bleak December. And each separate dying ember wrought its ghost upon the floor. Eagerly I wished the morrow vainly I had sought to borrow From my books surcease of sorrow—sorrow for the lost Lenore For the rare and radiant maiden whom the angels name Lenore Nameless here for evermore.",
                           title=None,
                           uid="doc_1_cluster_b"
                           )

doc_2_cluster_b = Document(body="Ah, distinctly perturbation  I remember it was   bleak December. And  separate dying ember  its ghost upon the floor.  I wished the morrow vainly I had sought to  From my books surcease of sorrow—sorrow for the  Lenore For the  and radiant maiden whom the angels name Lenore Nameless here for evermore.",
                           title=None,
                           uid="doc_2_cluster_b"
                           )


# Extract the cluster :
cluster = extrac_cluster([doc_1_cluster_a, doc_2_cluster_a, doc_1_cluster_b, doc_2_cluster_b], recipe, 0.2)
print(cluster)  # Check that documents are correctly clustered per cluster
