#! /usr/bin/python3

# monads_test.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Test of monads.py
"""

#############################################################################
#                                 Packages                                  #
#############################################################################
from cookbook.core.monads import (
    Nothing,
    Just,
    List
)

#############################################################################
#                                  Tests                                    #
#############################################################################


def test_explode_unnested():
    assert Just(1).explode() == 1
    assert List([]).explode() == []
    assert Nothing().explode() is None


def test_explode_nested_list():
    foo = List([Just(1)])
    assert foo.explode() == [1]

    foo = List([Just(1), List([Just(1), Just(2)])])
    assert foo.explode() == [1, [1, 2]]


def test_explode_nested_just():
    foo = List([Just(1), Just(2), Just(List([Just(1)]))])
    assert foo.explode() == [1, 2, [1]]

    foo = List([Just(1), Just(2), Just(List([Just(1), Just(List([Just(1)]))]))])
    assert foo.explode() == [1, 2, [1, [1]]]


def test_explode_primitive():
    foo = Just([1, 2, 3])
    assert foo.explode() == [1, 2, 3]
