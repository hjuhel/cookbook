#! /usr/bin/python3

# recipe_test.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Test of Recipe.py
"""

#############################################################################
#                                 Packages                                  #
#############################################################################
from cookbook.core import Recipe, Document
from cookbook.core.backend import PythonBackend
from cookbook.core.exceptions import NoStepsRegisteredError
from cookbook.core.flags import ScopeFlags
from cookbook.steps.preprocessing import StepCharngrams
from cookbook.core.monads import Just
from cookbook.steps import Step
from cookbook.steps.preprocessing import StepTokenize


import mock
import pytest
from pytest_mock import mocker
from unittest.mock import Mock

#############################################################################
#                                  Tests                                    #
#############################################################################


def test_no_provided_backend():
    with pytest.raises(TypeError):
        r = Recipe(1)


def test_no_steps(mocker):
    mocker.patch.object(PythonBackend, '__init__')
    PythonBackend.__init__.return_value = None
    with pytest.raises(NoStepsRegisteredError):
        r = Recipe(PythonBackend())
        r.prepare([Mock()])


def test_recipe_flag_inheritance():
    r_body = Recipe(PythonBackend(), ScopeFlags.BODY) + StepTokenize()
    r_title = Recipe(PythonBackend(), ScopeFlags.TITLE) + StepTokenize()
    r_both = Recipe(PythonBackend()) + StepTokenize()

    foo = Document(title='foo title', body='foo body')
    processed = r_body.prepare([foo])[0].to_native()
    assert processed.title == "foo title" and processed.body == ["foo", "body"]
    processed = r_title.prepare([foo])[0].to_native()
    assert processed.title == ["foo", "title"] and processed.body == "foo body"
    processed = r_both.prepare([foo])[0].to_native()
    assert processed.title == ["foo", "title"] and processed.body == ["foo", "body"]


def test_recipe_flag_override():
    r = Recipe(PythonBackend(), ScopeFlags.BODY) + StepTokenize(scope=ScopeFlags.TITLE)

    foo = Document(title='foo title', body='foo body')
    processed = r.prepare([foo])[0].to_native()
    assert processed.title == ["foo", "title"] and processed.body == "foo body"
