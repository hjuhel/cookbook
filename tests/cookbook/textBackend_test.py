#! /usr/bin/python3

# pythonBackend_test.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Test of python_backend.py
"""

#############################################################################
#                                 Packages                                  #
#############################################################################
import sys

from cookbook.core.backend import PythonBackend, SpacyBackend
from cookbook.core import Recipe, Document
from cookbook.core.monads import List, Just
from cookbook.core.flags import ScopeFlags
from cookbook.core.monads import Just, Nothing, List
from functools import lru_cache

from pytest import fixture
#############################################################################
#                                  Tests                                    #
#############################################################################


@lru_cache(maxsize=1)
@fixture
def spacyBackend():
    return SpacyBackend()


@lru_cache(maxsize=1)
@fixture
def pythonBackend():
    stopwords = {'eleven', 'somewhere', 'am', 'twelve', 'whither', 'down', 'yourselves', 'sometime', 'thence', 'must', 'again', 'every', 'mostly', 'may', 'he', 'four', 'really', 'say', 'therein', 'through', 'towards', 'several', 'of', 'therefore', 'where', 'alone', 'we', 'their', 'hers', 'though', 'as', 'full', 'none', 'name', 'along', 'any', 'seem', 'might', 'was', 'and', 'latter', 'our', 'there', 'becoming', 'seemed', 'keep', 'whenever', 'me', 'amongst', 'which', 'been', 'whose', 'other', 'thereupon', 'beyond', 'for', 're', 'perhaps', 'yours', 'these', 'together', 'former', 'upon', 'myself', 'them', 'meanwhile', 'hereafter', 'with', 'only', 'toward', 'done', 'show', 'neither', 'i', 'can', 'cannot', 'indeed', 'much', 'nor', 'onto', 'ten', 'then', 'become', 'in', 'take', 'thru', 'because', 'fifteen', 'seems', 'nothing', 'she', 'twenty', 'very', 'except', 'up', 'since', 'would', 'you', 'what', 'without', 'per', 'whence', 'first', 'this', 'to', 'serious', 'wherever', 'whoever', 'such', 'two', 'more', 'often', 'while', 'formerly', 'by', 'anyway', 'out', 'just', 'no', 'mine', 'that', 'whereafter', 'your', 'front', 'should', 'whether', 'last', 'noone', 'please', 'three', 'whom', 'third', 'above', 'beforehand', 'elsewhere', 'put', 'wherein', 'hundred', 'why', 'still', 'hence', 'well', 'see', 'when', 'next', 'becomes', 'afterwards', 'never', 'thereby', 'himself', 'something', 'top',
                 'even', 'go', 'almost', 'once', 'sixty', 'everything', 'thus', 'across', 'if', 'before', 'themselves', 'throughout', 'call', 'until', 'everywhere', 'do', 'already', 'has', 'side', 'anything', 'forty', 'not', 'are', 'be', 'amount', 'used', 'us', 'due', 'is', 'least', 'seeming', 'into', 'get', 'although', 'it', 'after', 'but', 'its', 'between', 'made', 'nowhere', 'how', 'doing', 'fifty', 'they', 'unless', 'a', 'nevertheless', 'herself', 'less', 'enough', 'empty', 'same', 'latterly', 'sometimes', 'among', 'him', 'yet', 'does', 'six', 'always', 'became', 'whatever', 'most', 'somehow', 'around', 'hereby', 'could', 'who', 'however', 'itself', 'whole', 'did', 'have', 'regarding', 'anyhow', 'now', 'off', 'otherwise', 'an', 'another', 'each', 'whereby', 'being', 'move', 'some', 'too', 'ca', 'had', 'various', 'below', 'my', 'everyone', 'here', 'or', 'ourselves', 'the', 'thereafter', 'will', 'his', 'many', 'against', 'whereas', 'one', 'others', 'were', 'either', 'both', 'using', 'back', 'from', 'part', 'her', 'behind', 'under', 'whereupon', 'also', 'few', 'someone', 'than', 'eight', 'during', 'on', 'yourself', 'anyone', 'at', 'further', 'hereupon', 'else', 'ours', 'via', 'five', 'own', 'herein', 'give', 'beside', 'nobody', 'nine', 'about', 'ever', 'within', 'besides', 'all', 'over', 'those', 'moreover', 'namely', 'make', 'quite', 'anywhere', 'so', 'bottom', 'rather'}

    return PythonBackend(stopwords=stopwords)


@fixture
def one_doc():
    return Document(Just('The first sentence, of the title. The second sentence.'), Just('The first sentence of the BODY !! The Second Sentece. Look ! Another sentence'))


@fixture
def list_docs():
    foo = Document(Just('The first sentence, of the title. The second sentence.'), Just('The first sentence of the BODY !! The Second Sentece. Look ! Another sentence'))
    return [foo, foo, foo]


def test_atomic_sentences(one_doc, spacyBackend, pythonBackend):
    processed = pythonBackend.sentences(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    assert processed.title.explode() == ["The first sentence, of the title.", 'The second sentence.']
    assert processed.body.explode() == ["The first sentence of the BODY !!", 'The Second Sentece.', 'Look !', 'Another sentence']

    processed = spacyBackend.sentences(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    assert processed.title.explode() == ["The first sentence, of the title.", 'The second sentence.']
    assert processed.body.explode() == ["The first sentence of the BODY !!", 'The Second Sentece.', 'Look !', 'Another sentence']


def test_atomic_tokenize(one_doc, spacyBackend, pythonBackend):
    processed = pythonBackend.tokenize(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    res = ["The", "first", "sentence", ",", "of", "the", "title", ".", "The", "second", "sentence", "."]
    assert processed.title.explode() == res

    processed = spacyBackend.tokenize(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    assert processed.title.explode() == res


def test_atomic_ngrams(one_doc, spacyBackend, pythonBackend):
    processed = pythonBackend.ngrams(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    assert processed.title.explode() == (('The', 'first', 'sentence'), ('first', 'sentence', 'of'), ('sentence', 'of', 'the'), ('of', 'the', 'title'),
                                         ('the', 'title', 'The'), ('title', 'The', 'second'), ('The', 'second', 'sentence'), ('second', 'sentence', ''), ('sentence', '', ''))

    processed = spacyBackend.ngrams(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    assert processed.title.explode() == (('The', 'first', 'sentence'), ('first', 'sentence', 'of'), ('sentence', 'of', 'the'), ('of', 'the', 'title'),
                                         ('the', 'title', 'The'), ('title', 'The', 'second'), ('The', 'second', 'sentence'), ('second', 'sentence', ''), ('sentence', '', ''))


def test_atomic_chargrams(one_doc, spacyBackend, pythonBackend):
    processed = pythonBackend.charngrams(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    foo = processed.title.explode()
    assert foo[0:7] == (('T', 'h', 'e'), ('h', 'e', ' '), ('e', ' ', 'f'), (' ', 'f', 'i'), ('f', 'i', 'r'), ('i', 'r', 's'), ('r', 's', 't'))
    assert foo[50:] == (('n', 'c', 'e'), ('c', 'e', '.'), ('e', '.', ''), ('.', '', ''))

    processed = spacyBackend.charngrams(one_doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    foo = processed.title.explode()
    assert foo[0:7] == (('T', 'h', 'e'), ('h', 'e', ' '), ('e', ' ', 'f'), (' ', 'f', 'i'), ('f', 'i', 'r'), ('i', 'r', 's'), ('r', 's', 't'))
    assert foo[50:] == (('n', 'c', 'e'), ('c', 'e', '.'), ('e', '.', ''), ('.', '', ''))


def test_atomic_removeStopWords(spacyBackend, pythonBackend):
    doc = Document(title='eleven somewhere I\'am a four really and platypus baby',
                   body='But there is no sense crying over every mistake. You just keep on trying until you run out of cake. And the science gets done. And you make a neat gun for the people who are still alive.')
    processed = spacyBackend.remove_stopWords(doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE)
    foo = processed.to_native()
    assert foo.title == 'I\'am platypus baby'
    assert foo.body == 'But sense crying mistake . You trying run cake . And science gets . And neat gun people alive .'

    foo = pythonBackend.remove_stopWords(doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE).to_native()

    assert foo.title == 'I \' platypus baby'
    assert foo.body == 'But sense crying mistake . You trying run cake . And science gets . And neat gun people alive .'


def test_nested_removeStopWords(spacyBackend, pythonBackend):
    doc = Document(title=List(["eleven", "somewhere", "I", "am", "a", "four", "really", "and", "platypus", "baby"]),
                   body=List(["But", "there", "is", "no", "sense", "crying", "over", "every", "mistake", "You", "just", "keep", "on", "trying", "until", "you", "run", "out", "of",
                              "cake", "And", "the", "science", "gets", "done", "And", "you", "make", "a", "neat", "gun", "for", "the", "people", "who", "are", "still",  "alive"])
                   )

    processed = spacyBackend.remove_stopWords(doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE).to_native()
    assert processed.title == '[ , , I , , , , , , platypus , baby ]'
    assert processed.body == '[ But , , , , sense , crying , , , mistake , You , , , , trying , , , run , , , cake , And , , science , gets , , And , , , , neat , gun , , , people , , , , alive ]'

    processed = pythonBackend.remove_stopWords(doc, scope=ScopeFlags.BODY | ScopeFlags.TITLE).to_native()

    assert processed.title == '[eleven , , I , , , , , , platypus , baby]'
    assert processed.body == '[But , , , , sense , crying , , , mistake , You , , , , trying , , , run , , , cake , And , , science , gets , , And , , , , neat , gun , , , people , , , , alive]'


if __name__ == "__main__":
    sys.exit()
