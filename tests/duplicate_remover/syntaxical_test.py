#! /usr/bin/python3

# syntacical_test.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Test the syntaxical deduplication
"""

#############################################################################
#                                 Packages                                  #
#############################################################################
import sys

from duplicates_remover.app.syntaxical import post_volatil_syntaxical
from cookbook.core import Document

import asyncio
import pytest
import pytest_asyncio


#############################################################################
#                                  Tests                                    #
#############################################################################

@pytest.mark.asyncio
async def test_syntaxical_volatil_atomic_input():
    res = await post_volatil_syntaxical([Document('a', 'a', uid='a')], 100, 50)
    assert res == ['a']


@pytest.mark.asyncio
async def test_syntaxical_volatil_cluster():
    a1 = Document(title=None, body='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum in urna.', uid='a1')
    a2 = Document(title=None, body='Lorem sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum urna.', uid='a2')
    b1 = Document(title=None, body='Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.', uid='b1')
    b2 = Document(title=None, body='aaaaa Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.', uid='b2')

    res = await post_volatil_syntaxical([a1, a2, b1, b2], 200, 50)
    res = sorted([sorted(item) for item in res])
    assert res == [['a1', 'a2'], ['b1', 'b2']]


if __name__ == "__main__":
    sys.exit()
