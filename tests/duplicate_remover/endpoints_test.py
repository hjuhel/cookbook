#! /usr/bin/python3

# endpoints_test.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Test the main endpoints of the app
"""

#############################################################################
#                                 Packages                                  #
#############################################################################
import sys
from duplicates_remover.app.main import app
from starlette.testclient import TestClient
import json

# Instanciate a client to be tested
client = TestClient(app)


#############################################################################
#                                  Script                                   #
#############################################################################

def test_post_volatil_empty():

    response = client.post('/volatil/syntaxical/')
    assert response.status_code == 422


def test_post_volatil_expected():
    payload = [
        {
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum in urna.",
            "uid": "a1"
        },
        {
            "body": "Lorem sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum urna.",
            "uid": "a2"
        },
        {
            "body": "Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.",
            "uid": "b1"
        },
        {
            "body": "aaaaa Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.",
            "uid": "b2"
        }
    ]
    response = client.post('/volatil/syntaxical/', data=json.dumps(payload))
    assert response.status_code == 200
    response = response.json()
    response = sorted([sorted(item) for item in response])
    assert response == [['a1', 'b1', 'b2'], ['a2']]


def test_post_volatil_custom_params():
    payload = [
        {
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum in urna.",
            "uid": "a1"
        },
        {
            "body": "Lorem sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum urna.",
            "uid": "a2"
        },
        {
            "body": "Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.",
            "uid": "b1"
        },
        {
            "body": "aaaaa Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.",
            "uid": "b2"
        }
    ]

    params = {'signature_size': '200', 'duplication_threshold': '50'}
    response = client.post('/volatil/syntaxical/', params=params, data=json.dumps(payload))
    assert response.status_code == 200
    response = response.json()
    response = sorted([sorted(item) for item in response])
    assert response == [['a1', 'a2'], ['b1', 'b2']]


def test_post_volatil_none_body():
    payload = [
        {
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum in urna.",
            "uid": "a1"
        },
        {
            "body": "Lorem sit amet, consectetur adipiscing elit. Cras id urna vel urna ullamcorper sodales. Curabitur maximus ipsum urna.",
            "uid": "a2"
        },
        {
            "body": "",
            "uid": "b1"
        },
        {
            "body": "aaaaa Donec sed sem euismod, bibendum ligula at, tempus leo. Nullam dui urna.",
            "uid": "b2"
        }
    ]

    response = client.post('/volatil/syntaxical/', data=json.dumps(payload))
    assert response.status_code == 457
    response = response.json()['detail']
    assert response == "At least one body field of the provided documents wasn't a string or was None."


if __name__ == "__main__":
    sys.exit()
