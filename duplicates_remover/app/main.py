#! /usr/bin/python3

# main.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
    Implements the main API for the syntaxical duplicate parser
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import logging
from typing import (
    List
)

# Additional packages
from unicodedata import normalize
from fastapi import FastAPI, HTTPException
from fastapi.openapi.utils import get_openapi
import uvicorn
from oslash import Maybe

# Project related packages
from .models import ItemDocument, ItemDuplicate, ItemDuplicateList, Language
from .syntaxical import post_syntaxical_duplicate, get_syntaxical_duplicate, post_volatil_syntaxical
from cookbook.core import Document
from cookbook.core.monads import Just, Nothing

#############################################################################
#                                   Script                                  #
#############################################################################

# Helpers function


def _normalized_monoid_factory(self, text: str) -> str:
    """
    Custom monoid factory to add normalization for the Document process creation.
    The document factory's method is designed to be monkey patched, so this is not that ugly. 
    """

    if not text:
        out = Nothing()
    elif isinstance(text, Maybe):
        out = text
    else:
        try:
            out = Just(normalize('NFKD', str(text)))
        except TypeError:
            out = Nothing()

    return out


Document._text_factory = _normalized_monoid_factory

# Instanciate a main app object
app = FastAPI()


# Monkey patch the API legend generator to add custom information
def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Syntaxical and semantical duplicate remover",
        version="0.0.1",
        description="",
        routes=app.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


# Register routes
@app.post("/permanent/syntaxical/", response_model=ItemDuplicate, status_code=201)
async def _post_syntaxical_duplicate(item: ItemDocument, language: Language = Language.ENGLISH):
    """
    The /permanent's methods enables and leverage data persistence
    Check if the provided document has some already-registred duplicates in the database.

    Returns:
        Boolean - True, if the document or a document's duplicate is already enregistred in the database. False otherwise.
    """

    # Transfrom and normalize the json to a Document
    try:
        item_dict = item.dict()
        item_document = Document(**item_dict)
    except BaseException as error:
        logging.error(error)
        raise HTTPException(status_code=500, detail=f"Input sanitization failed with error message : {error}")

    # Check for duplicate
    item_duplicate_status = await post_syntaxical_duplicate(item_document)
    return {"uid": item_document.uid, "duplicate": item_duplicate_status}


@app.get("/permanent/syntaxical/{document_uid}", response_model=ItemDuplicateList, status_code=200)
async def _get_syntaxical_duplicate(document_uid: str):
    """
    The /permanent's methods enables and leverage data persistence
    Get a list of all syntaxical duplicates for a specific document

    Returns:
        Boolean - True, if the document or a document's duplicate is already enregistred in the database. False otherwise.
    """

    try:
        duplicates_uids = await get_syntaxical_duplicate(document_uid)
    except HTTPException as error:
        logging.error(error)
        raise error
    except BaseException as error:
        logging.error(error)
        raise HTTPException(status_code=500, detail=f"Failed to retrieve the document\'s uid duplicate with error {error}")

    return {"uid": document_uid, "duplicateList": duplicates_uids}


@app.post("/volatil/syntaxical/", response_model=List[List[str]], status_code=200)
async def _post_syntaxical_duplicate(items: List[ItemDocument], language: Language = Language.ENGLISH, signature_size: int = 100, duplication_threshold: int = 50):
    """
    On batch removal of duplicates.
    Documents ID and hash won't be stored in the database.
    Returns cluster of syntaxically similar documents.
    """

    if duplication_threshold >= signature_size:
        raise HTTPException(status_code=400, detail=f"Signature_size param must be larger than duplication_threshold")
    if duplication_threshold <= 0 or signature_size <= 0:
        raise HTTPException(status_code=400, detail=f"Signature_size and duplication_threshold params must be strictly positive")
    if len(items) <= 1:
        raise HTTPException(status_code=400, detail=f"The list of documents to compare must be strictly positive")

    # Transfrom and normalize the json to a list of documents object
    try:
        # Map the payload to a tuple of Document objects
        item_list = tuple(Document(**item.dict()) for item in items)
    except TypeError:
        raise HTTPException(status_code=457, detail=f"At least one body field of the provided documents wasn\'t a string or was None.")
    except BaseException as error:
        logging.error(error)
        raise HTTPException(status_code=500, detail=f"Input sanitization failed with error message : {error}")

    # Extract cluster from the list of documents
    clusters_list = await post_volatil_syntaxical(item_list, signature_size, duplication_threshold)
    return clusters_list

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)
