#! /usr/bin/python3

# data_validation.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
    Describe base models to map the queries onto Cookbook items
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import List
from enum import Enum

# Additional packages
from pydantic import BaseModel

# Project related packages

#############################################################################
#                                  Classes                                  #
#############################################################################


class Language(str, Enum):
    """
    The langague of the document. For now, only english is supported.
    """
    ENGLISH = 'english'


class ItemDocument(BaseModel):
    """
    title: The title of the document
    body: The body the document. Required
    uid: An optionnal unique id for the document. If no uid is provided, then a uid will be generated as a md5 hash on the body
    """
    title: str = None
    body: str
    uid: str = None


class ItemDuplicate(BaseModel):
    """
    uid: The uid of the processed document. The uid is either the one from the post request (if provided), either the md5 hash of the POST's request body.
    duplicate: A boolean set to True if the document is a duplicate of an already existing document, False otherwise.
    """

    uid: str
    duplicate: bool


class ItemDuplicateList(BaseModel):
    """
    uid: The uid of the processed document. The uid is either the one from the post request (if provided), either the md5 hash of the POST's request body.
    duplicateList: A list of document's uid. One for each input's duplicate
    """

    uid: str
    duplicateList: List[str]


if __name__ == "__main__":
    sys.exit()
