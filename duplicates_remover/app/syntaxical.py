#! /usr/bin/python3

# syntaxical.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
    Implementations of the syntaxical duplicates functions called from the main app
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
from collections import Counter, defaultdict
import sys
from typing import List
from itertools import chain
from functools import lru_cache

# Additional packages
from sqlitedict import SqliteDict
from fastapi import HTTPException

# Project related packages
from cookbook.core.backend import PythonBackend
from cookbook.core import Document, Recipe
from cookbook.core.flags import ScopeFlags
from cookbook.steps.preprocessing import StepNgrams
from cookbook.steps.hash import StepMinHash
from .models import ItemDocument


#############################################################################
#                                  Script                                   #
#############################################################################


# Utility functions


# Define a Recipe to encode the text
_SYNTAXICAL_RECIPE = Recipe(PythonBackend(), scope=ScopeFlags.BODY) + \
    StepNgrams() + \
    StepMinHash(signature_size=100)

_THRESHOLD_JACQUARD = 50


@lru_cache(maxsize=5)
def _retrieve_recipe(signature_size: int):
    """
    Cache the recipe based on the signature size to avoid regenerating seeds and to allow for multiple
    signature comparaison

    Arguments:
        signature_size {Int} -- The size of the encoding

    # TODO: The cache must be improved using a two level cache, one for the Backendm one for the Siganture. Also it schould be properly done in a dedicated module using dict. LRU is overkill !
    """
    return Recipe(PythonBackend(), scope=ScopeFlags.BODY) + \
        StepNgrams(size=5) + \
        StepMinHash(signature_size=signature_size)

# Endpoints


async def post_syntaxical_duplicate(doc: Document) -> bool:
    """
    Check if the provided document object has a registered duplicate in the database

    Arguments:
        doc {Document} -- The document to to process

    Returns:
        bool -- True if the document has a registered duplicate. False Otherwise.

    # TODO: Test and deal with polar/edges cases
    # TODO: Add support for handling the jacquart param in the method call
    """

    # Encode the document using the prepared database
    processed = _SYNTAXICAL_RECIPE.prepare(doc)

    # Extract the body from the the docs
    processed = processed.to_native()

    # Check the document hash against the database
    with SqliteDict('/var/duplicationDictionnaries/keys2docs.qlite') as archive:
            # Count the number of times the most common other key appears
        try:
            _, occurences = Counter(subitem for item in (item for item in (archive.get(key, None) for key in processed.body) if item is not None) for subitem in item).most_common(1)[0]
            duplicate = occurences >= _THRESHOLD_JACQUARD
        except BaseException:
            duplicate = False

        # Store the document reference into the buckets
        for key in processed.body:
            try:
                switch_documents_set = archive[key]  # Because of SQLLiteDIct interactions with sets, i have to use a temp variable
                switch_documents_set.add(processed.uid)
                archive[key] = switch_documents_set
            except KeyError:
                archive[key] = set([processed.uid])

        archive.commit()

    # Save the keys of the document in the docs2keys dict
    with SqliteDict('/var/duplicationDictionnaries/docs2keys.qlite') as archive:
        archive[processed.uid] = processed.body
        archive.commit()

    return duplicate

    # ------------------------------------------------------------ #


async def get_syntaxical_duplicate(uid: str) -> List[str]:
    """
    Retrieve a list of syntaxical duplicates for a document's uid

    Arguments:
        uid {str} -- The uid of the document to retrieve the duplicates from

    Returns:
        List[str] -- A uid list of duplicated document

    # TODO: Test and deal with polar/edges cases
    """

    # Retrieve the keys of the document
    with SqliteDict('/var/duplicationDictionnaries/docs2keys.qlite') as archive:
        try:
            keys = archive[uid]
        except KeyError:
            raise HTTPException(status_code=404, detail='The uid\'s does not belongs to any ressource.')

    # Get all the duplicates for the keys
    with SqliteDict('/var/duplicationDictionnaries/keys2docs.qlite') as archive:
        # Count the number of times others documents occurs
        counter = Counter(chain.from_iterable(archive[key] for key in keys))

    # Iterate over counter and keep the items having more occurencies than the one specified in the threshold
    duplicates_uids = (item[0] for item in counter.items() if item[1] >= _THRESHOLD_JACQUARD)

    return duplicates_uids


async def post_volatil_syntaxical(items: List[ItemDocument], signature_size: int, duplication_threshold: int) -> List[List[str]]:
    """
    Clusterize items into groups based on the detected number of cluster

    Arguments:
        items {List[ItemDocument]} -- The list of documents to process
        signature_size {int} -- The signature to use for the encoding
        duplication_threshold {int} -- The threshold to use for the jacquard index to consider two documents as duplicates.

    Returns:
        List[List[str]] -- A list of cluster. Each cluster consists in a list of document's uid
    """

    # Extract the hash from the texts
    recipe = _retrieve_recipe(signature_size)  # Retrieve / generate the signature using the cache
    signatures = recipe.prepare(items)
    signatures = tuple(item for item in (item.to_native() for item in signatures) if item is not None)  # Only keep the body: Nothing() from corpus

    # Deal with polar cases
    if not signatures:
        return []
    if len(signatures) == 1:
        return [signatures[0].uid]

    # Build a key 2 doc to avoid useless comparison : Only document sharing at one least one key will be compared together
    doc2key = defaultdict(set)
    for doc in signatures:
        for key in doc.body:
            doc2key[key].add(doc.uid)

    # Traverse all documents to retrieve potential duplicates for a specific document
    clusters = []  # A list of retrieved clusters
    for doc in signatures:
        # Check if the doc has already been added in a cluster, if so go the next document:
        if doc.uid in chain.from_iterable(clusters):
            continue

        counter = Counter(chain.from_iterable(doc2key[key] for key in doc.body))  # Build an occurencies counter of coocurences between the docs and all others docs
        cluster = {item[0] for item in counter.items() if item[1] >= duplication_threshold}  # Select the docs'ID for which the occurences is larger than the threshold
        cluster.add(doc.uid)  # Add the uid of document acting as a reference
        clusters.append(list(cluster))

        # Remove the already added document from the dict :
        # The left difference is the the key that might be shared by other documents than with the reference doc, so i can't just remove the dict's key
        # Remove the ref document
        for key in doc.body:
            doc2key[key] = doc2key[key] - cluster  # Even if the new set is empty, the key is preserved to avoid dealing with KeyError in the counter (it's faster)

    return clusters

if __name__ == "__main__":
    sys.exit()
