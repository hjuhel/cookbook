#! /usr/bin/python3

# vicinityDriven_aligner.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implement the vicinityDriven aligner
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys

# Additional packages
from cookbook.core import Document, Recipe
from cookbook.core.backend import PythonBackend
from cookbook.core.flags import ScopeFlags
from cookbook.steps.preprocessing import StepSentences

import numpy as np

# Project related packages
from copycat.similarity.similarity_model import SimilarityModel
from .aligner_model import AlignerModel

#############################################################################
#                                  Classes                                  #
#############################################################################


class VicinityDrivenAligner(AlignerModel):
    """
    Implements the vicinity driven aligner
    """

    def __init__(self, similarity_model: SimilarityModel, acceptable_similarity: float = 0.2, similarity_slack: float = 0.05):
        """
        Instanciate a vicinity driven aligner

        Arguments:
            similarity_model {SimilarityModel} -- The similarity model to use to compute the similarity between texts

        Keyword Arguments:
            acceptable_similarity {float} -- The minimun similarity model score between two sentences for an alignement to be considered (default: {0.2})
            similarity_slack {float} --The maximum amount of similarity that can be lost after each step of N when finding for a 1-N or N-1 alignment (default: {0.05})
        """

        # Params affectation
        self._similarity_model = similarity_model
        self._acceptable_similarity = acceptable_similarity
        self._similarity_slack = similarity_slack

        # Vicinity affectation
        self._first_vicinity = set(((1, 1), (1, 0), (0, 1), (2, 1), (1, 2)))
        self._second_vicinity = set(((1, 2), (2, 1)))
        self._total_vicinity = self._first_vicinity + self._second_vicinity

        # Cookbook recipe for text preprocessing
        self._to_sentences = Recipe(PythonBackend(), scope=ScopeFlags.BODY) + StepSentences()

    # ---------------------------------------------- # Protocols

    def __call__(self, text_1: str, text_2: str):
        """
        Compute the best sentences alignements between text_1 and text_2

        Keyword Arguments:
            text_1 {str} -- The reference text
            text_2 {str} -- The text to align to the reference
        """

        # Transform the text to Documents objects
        docs = map(lambda x: Document(title=None, body=x))

        # Split the texts into sentences
        sents = self._to_sentences.prepare(docs)

        # Compute the similarity
        sentences_similarity = self._distance_model(*sents)

        # Calculate the alignement path
        alignement_path = self._compute_alignement_path(sentences_similarity)

    # ---------------------------------------------- # Private

    def _compute_alignement_path(self, sentences_similarity: np.array):
        """
        Search the best alignement path within a similarity matrix

        Arguments:
            sentences_similarity {np.array} -- The similarity matrix from which the best alignement must be found
        """

        # Locals params instanciation
        # Extract the number of sentences
        len_p1, len_p2 = sentences_similarity.shape

        # Placeholders intanciation
        path = []

        # Find starting point
        starting_point = self._get_starting_point(sentences_similarity, len_p1, len_p2, (-1, -1))

        return 1

    def _get_starting_point(self, sentences_similarity, len_p1: int, len_p2: int, starting_position):
        """
        Searches for coordinate in the similarity matrix from which start or recover the alignement path

        Arguments:
            sentences_similarity {[type]} -- The similarity matrix to use for the distance computation
            len_p1 {int} -- The  number of sentences in the first document
            len_p2 {int} -- The number of sentences in the second document 
            starting_position {tuple} -- The starting position from which the starting point must be found
        """

        # Placeholders instanciation
        visited = []
        current_distance = 0
        found: bool = False  # True if the good enough pair has been found
        ended: bool = False  # True if all pairs has been exhausted without having find a good enough pair
        current_position = starting_position

        # Iterate over the matrix until a good enough pair is found
        while not found and not ended:

            if current_position == (-1, -1):  # If the position is unknow, then start by the first possible alignement
                current_position = (0, 0)
            else:


if __name__ == "__main__":
    sys.exit()
