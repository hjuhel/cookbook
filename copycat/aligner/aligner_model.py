#! /usr/bin/python3

# aligner_model.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Implement an interface for the Aligner Algorithms
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from abc import (
    ABC,
    abstractmethod
)

# Additional packages

# Project related packages
from copycat.similarity.similarity_model import SimilarityModel

#############################################################################
#                                  Classes                                  #
#############################################################################


class AlignerModel(ABC):
    """
    Interface. Define a unique interface for performing model comparison
    """

    __slots__ = ('_distance_model', )

    def __init__(self, distance_model: SimilarityModel, acceptable_similarity: float, ** kwargs):
        """
        Instanciate and configure the aligner

        Arguments:
            distance_model {DistanceModel} -- The distance model to use to conpute the distance between two sets of sentences
        """

        self._distance_model = distance_model
        self._acceptable_similarity = acceptable_similarity

    # ---------------------------------------------- # Getters / setters

    @property
    def distance_model(self):
        """
        Getter for the distance_model attribute

        Returns:
            DistanceModel -- The injected distance model the Aligner relays on
        """

        return self._distance_model

    @distance_model.setter
    def distance_model(self, value: SimilarityModel):
        """
        Setter for the distance_model attribute

        Arguments:
            value {DistanceModel} -- The injected distance model the Aligner relays on
        """

        self._distance_model = value

    # ---------------------------------------------- #

    @property
    def acceptable_similarity(self) -> float:
        """
        Acceptable similarity's atrribute getter

        Returns:
            float -- The minimal acceptable similarity
        """

        return self._acceptable_similarity

    # ---------------------------------------------- # Protocols

    @abstractmethod
    def __call__(self, text_1: str, text_2: str, *kwargs):
        """
        Perform the alignement between two texts

        Arguments:
            text1 {str} -- The reference text
            text2 {str} -- The text to compare

        Returns:
            # TODO : determines return types
        """


if __name__ == "__main__":
    sys.exit()
