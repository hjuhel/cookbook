#! /usr/bin/python3

# residual_aligner.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
"""
Implement the residual aligner. The residual aligner is a naïve but fast approch to sentence alignement.
The algorithms align one sentence from the reference text to one text from the target reference.
The algorithms works recursively
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from typing import Tuple
from itertools import product

# Additional packages
import numpy as np
from cookbook.core import Document, Recipe
from cookbook.core.backend import PythonBackend
from cookbook.core.flags import ScopeFlags
from cookbook.steps.preprocessing import StepSentences

# Project related packages
from copycat.similarity.similarity_model import SimilarityModel
from .aligner_model import AlignerModel

#############################################################################
#                                  Classes                                  #
#############################################################################


class ResidualAligner(AlignerModel):
    """
    Implements the residual aligner
    """

    __slots__ = ('_similarity_model', '_acceptable_sinilarity', '_span')

    def __init__(self, similarity_model: SimilarityModel, acceptable_similarity: float = 0.2, max_span: int = None):
        """
        Instanciate a residual aligner object

        Arguments:
            similarity_model {SimilarityModel} -- The similarity model to use to compute the similarity between texts

        Keyword Arguments:
            acceptable_similarity {float} -- The minimun similarity model score between two sentences for an alignement to be considered (default: {0.2})
        """

        # Params affectation
        self._similarity_model = similarity_model
        self._acceptable_similarity = acceptable_similarity
        if max_span is not None:
            self._span = int(max_span)
        else:
            self._span = None

        # Cookbook recipe for text preprocessing
        self._to_sentences = Recipe(PythonBackend(), scope=ScopeFlags.BODY) + StepSentences()

        # par

    # ---------------------------------------------- # Protocols

    def __call__(self, text_1: str, text_2: str):
        """
        Compute the best sentences alignements between text_1 and text_2

        Keyword Arguments:
            text_1 {str} -- The reference text
            text_2 {str} -- The text to align to the reference
        """

        # Transform the text to Documents objects
        docs = map(lambda x: Document(title=None, body=x), (text_1, text_2))

        # Split the texts into sentences
        sents = self._to_sentences.prepare(docs)

        # Compute the similarity
        sentences_similarity = self._similarity_model(*sents)

        # Calculate the alignement path
        alignement_path, unmatched_1, unmatched_2 = self._compute_alignement_path(sentences_similarity)

        # Convert sentenctes to actual string lists
        sents = map(lambda x: x.body.explode(), sents)
        sents_1, sents_2 = sents

        # Pair sentences
        pairs = tuple((sents_1[item[0]], sents_2[item[1]]) for item in alignement_path)
        unmatched_1 = tuple(sents_1[item] for item in unmatched_1)
        unmatched_2 = tuple(sents_2[item] for item in unmatched_2)

        return pairs, unmatched_1, unmatched_2

    # ---------------------------------------------- # Private

    def _compute_alignement_path(self, matrix: np.array):
        """
        Search the best alignement path using the sentence_similarity matrix
        Arguments:matrix_list
            matrix {np.array} -- The matrix_liste similarity matrix from which the best alignement must be found

        # TODO: Do it properly without mutating variables everywhere ....
        """

        # Locals params instanciation
        # Extract the number of sentences, and keep a MUTABLE ref to the lists
        len_p1, len_p2 = matrix.shape
        p1_range = [item for item in range(len_p1)]
        p2_range = [item for item in range(len_p2)]

        # Remove out of spans item
        if self._span:
            to_remove = filter(lambda x: abs(x[0] - x[1]) > self._span, product(*map(lambda x: list(range(x)), matrix.shape)))

            for item in to_remove:
                matrix[item[0], item[1]] = 0

        # Placeholders intanciation
        match_path = []
        sim_path = []

        # Match sentences
        for _ in range(matrix.shape[0]):

            argmax = np.argmax(matrix)
            coord = np.unravel_index(argmax, matrix.shape)
            sim = matrix[coord[0], coord[1]]

            if sim < self._acceptable_similarity:  # Stopping condition : is the similarity too low ?
                break

            # Register the new position
            pair = p1_range[coord[0]], p2_range[coord[1]]
            match_path.append(pair)
            sim_path.append(sim)

            # Remove the pair from the lsit of sentences
            del p1_range[coord[0]]
            del p2_range[coord[1]]

            # Remove from the matched column and rows from the matrix
            matrix = np.delete(matrix, coord[0], 0)
            matrix = np.delete(matrix, coord[1], 1)

        return match_path, p1_range, p2_range


if __name__ == "__main__":
    sys.exit()
