#! /usr/bin/python3

# jacquard_sinilarity.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Concrete implementation of the similarity_model using cookbook's jacquard computation ;) 
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from itertools import product

# Additional packages
import numpy as np
from sklearn.metrics import jaccard_score
from cookbook.core import Document, Recipe
from cookbook.core.backend import TextBackend
from cookbook.steps.preprocessing import StepCharngrams
from cookbook.steps.hash import StepMinHash
from cookbook.core.flags import ScopeFlags

# Project related packages
from .similarity_model import SimilarityModel

#############################################################################
#                                  Classes                                  #
#############################################################################


class JaccardSimilarity(SimilarityModel):
    """
    Implements the computation of the jaccard distance index, using the cookbook's min hash algorithms
    """

    __slots__ = ('_recipe')

    def __init__(self, backend: TextBackend, chargrams=3, signature_size=100):
        """Instanciate and configure the similarity model
        """

        # Configure the recipe to preprocess the documents
        r = Recipe(backend, scope=ScopeFlags.BODY)  # Only consider the body
        r + StepCharngrams(size=chargrams)  # For each sentences, compute a list of charngrams
        r + StepMinHash(signature_size=signature_size)  # Encode each sentence into their corresponding Min Hash

        self._recipe = r

    # ---------------------------------------------- #

    def __call__(self, text_1: Document, text_2: Document, **kwargs) -> np.array:
        """
        Compute the similarity between the sentences of text 1 and sentences of text 2

        Arguments:
            text1 {Document} -- The reference text, as a Cookbook object. Title will be ignored
            text2 {Document} -- The text to compare, as a Cookbook object. Title will be ignored

        Returns:
            np.array -- The similarity matrix between text_1 and text_2, of dims (len(text_1), len(text_2))
        """

        # Encode the document:
        encoded = self._recipe.prepare((text_1, text_2))

        # Get the sentence's vectors
        encoded = tuple(map(lambda x: x.body.explode(), encoded))  # Transform the monads, into natural python's list

        # Build the distance matrix
        matrix = []
        for item in product(*encoded):
            matrix.append(jaccard_score(*item, average="micro"))

        # Convert the vector to a numpy matrix
        matrix = np.array(matrix)

        # Reshape the vector to a similarity matrix
        shapes = tuple(map(len, encoded))
        matrix = matrix.reshape(shapes)

        return matrix


if __name__ == "__main__":
    sys.exit()
