#! /usr/bin/python3

# distance_model.py
#
# Project name: Caisse Dépôts et Placements du Québec.
# Author: Hugo Juhel
#
# description:
""" 
Implement an interface for the distance Model
"""

#############################################################################
#                                 Packages                                  #
#############################################################################

# System packages
import sys
from abc import (
    ABC,
    abstractmethod
)

# Additional packages
import numpy as np

# Project related packages

#############################################################################
#                                  Classes                                  #
#############################################################################


class SimilarityModel(ABC):
    """
    Interface. Define a unique interface for models perfgorming Similarity comparison between texts.
    """

    @abstractmethod
    def __init__(self, **kwargs):
        """Instanciate and configure the Similarity model
        """

        raise NotImplementedError("Must be implemented in the concrete class")

    # ---------------------------------------------- #

    @abstractmethod
    def __call__(self, text_1: str, text_2: str, *kwargs) -> np.array:
        """
        Compute the Similarity between the sentences of text 1 and sentences of text 2

        Arguments:
            text1 {str} -- The reference text
            text2 {str} -- The text to compare

        Returns:
            np.array -- The Similarity matrix between text_1 and text_2, of dims (len(text_1), len(text_2))
        """


if __name__ == "__main__":
    sys.exit()
