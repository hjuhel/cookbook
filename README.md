Cookbook, duplicate remover, copycat
====================================

I'have been working on this stack for the last two weeks : 

# Intro : 
## Cookbook
Cookbook provide an abstraction over Spacy / NLTK / GENSIM to performs transformation and preprocessings on texts.
For now, only Spacy and Pure Python are supported. 

## Duplicate_remover
A Dockerized app to clusterize text base on their syntaxical similarities. The app heavily relays on Cookbook for the preprocessing. This app is the reason i have decided to start building cookbook

## Copycat
A library to perform sentences alignements between two texts. The library also relays on cookbook. The library is a draft, as i have only been working on it for one day


# How to :
You can browse this repository to check my programming skills (or my lack of skills). Please keep in mind that all this work is an ongoing work, only starded few weeks ago. 
* Read the ```demo.cookbook.py``` file to see how one can use cookbook.
* Read the ```cookbook``` library for an overwiew of my programming skills
* Read the ```duplicate_remover``` app to assess my API skills and my ability to build Dockerized app.

hugo juhel.

