FROM tiangolo/uvicorn-gunicorn-fastapi:python3.6-alpine3.8

# Create and set up required directories
RUN mkdir -p /app/library
RUN mkdir -p /var/duplicationDictionnaries
RUN chmod -R 777 /var/duplicationDictionnaries
WORKDIR /app

# Install dependencies
RUN apk add --no-cache --allow-untrusted --repository http://dl-3.alpinelinux.org/alpine/edge/testing hdf5 hdf5-dev
RUN apk --no-cache --update-cache add gcc gfortran python python-dev py-pip build-base wget freetype-dev libpng-dev openblas-dev
ADD duplicates_remover/requirements.txt $WORKDIR/requirements.txt
RUN pip install --no-cache-dir --trusted-host pypi.org -r $WORKDIR/requirements.txt && rm $WORKDIR/requirements.txt
RUN python -m spacy download en_core_web_sm

# Copy code to the app directory
ADD duplicates_remover/app $WORKDIR

# Copy the libraries code to the library folder
ADD cookbook /app/library/cookbook

# Export path
ENV PATH ="${PATH}:/app/library"
ENV PYTHONPATH "${PATH}:/app/library"
ENV BIND "0.0.0.0:80"


